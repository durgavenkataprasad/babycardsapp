//
//  YAxisLabel.swift
//  BigFiveDiscipline
//
//  Created by Sheereen Thowlath on 20/03/18.
//  Copyright © 2018 Openwave. All rights reserved.
//

import UIKit
@IBDesignable
class YAxisLabel: UILabel {

    override func draw(_ rect: CGRect) {
        guard let text = self.text else {
            return
        }
        
        // Drawing code
        if let context = UIGraphicsGetCurrentContext() {
            let transform = CGAffineTransform( rotationAngle: CGFloat(-Double.pi/2))
            context.concatenate(transform)
            context.translateBy(x: -rect.size.height, y: 0)
            var newRect = rect.applying(transform)
            newRect.origin = CGPoint.zero
            
            let textStyle = NSMutableParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
            textStyle.lineBreakMode = self.lineBreakMode
            textStyle.alignment = self.textAlignment
            
            let attributeDict: [NSAttributedStringKey: AnyObject] = [NSAttributedStringKey.font: self.font, NSAttributedStringKey.foregroundColor: self.textColor, NSAttributedStringKey.paragraphStyle: textStyle]
            
            let nsStr = text as NSString
            nsStr.draw(in: newRect, withAttributes: attributeDict)
        }
    }
}
