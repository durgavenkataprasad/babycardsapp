//
//  BabyGirlTwoViewController.swift
//  BabyCards
//
//  Created by CIPL108-MOBILITY on 23/08/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class BabyGirlTwoViewController: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var scrollImg: UIScrollView!
    @IBOutlet weak var imgGirl2: UIImageView!
    var girl2Info = [String:String]()
    var imagePicker = UIImagePickerController()

    @IBOutlet weak var screenShotView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setScrollVwDelegate()
        self.displayGirl2Info()
    }
    func displayGirl2Info(){
        self.navigationItem.title = "Baby"
        self.lblName.text =  girl2Info["babyName"]!.uppercased()
        let spiltArray = girl2Info["babyDate"]?.components(separatedBy: "/")
        self.lblDate.text = spiltArray![0] + "." + spiltArray![1] + "." +  spiltArray![2]
        self.lblTime.text = girl2Info["babyTime"]! + " " + girl2Info["babyTimeZone"]!
        lblWeight.text = girl2Info["babyWeight"]! + " " + girl2Info["babyWieghtType"]!
        lblHeight.text = girl2Info["babyHeight"]!  + " " + girl2Info["babyHeightType"]!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func setScrollVwDelegate(){
        scrollImg.delegate = self
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgGirl2
    }
     @IBAction func actionTakePhoto(_ sender: Any) {
        openCamera()
    }
      @IBAction func actionShare(_ sender: Any) {
        let image: UIImage? = screenShotView.takeScreenshot()
        if image != nil {
            let imageToShare = [image!]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert!", message: "Please Take or Choose photo", controller: self)
        }
    }
    @IBAction func actionPhotoLibrary(_ sender: Any) {
        openGallary()
    }
  }
extension BabyGirlTwoViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
         }else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallary(){
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        //You will get cropped image here..
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.imgGirl2.image = image
        }else{
            print("Something went wrong")
        }
    }
}
