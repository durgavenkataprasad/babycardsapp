//
//  BabyBoyTextOneViewController.swift
//  BabyCards
//
//  Created by Praveenkumar R on 10/08/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class BabyBoyTextOneViewController: UIViewController {
    
    @IBOutlet weak var lblBabyName: UILabel!
    @IBOutlet weak var lblParentName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var mainVw: UIView!
    @IBOutlet weak var lblHeightType: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblWeightType: UILabel!
    @IBOutlet weak var lblweight: UILabel!
    @IBOutlet weak var lblBornTime: UILabel!
    var babyInfo = [String:String]()
    @IBOutlet weak var lblBornTimeZone: UILabel!
    @IBOutlet weak var screenShotVw: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.DisplayBabyInfo()
        self.setBoarderColor()
    }
    func setBoarderColor()
    {        mainVw.layer.borderWidth = 3.0
        mainVw.layer.borderColor = UIColor (red: 122.0/255.0, green: 222.0/255.0, blue: 228.0/255.0, alpha: 1.0).cgColor
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func DisplayBabyInfo(){
        self.navigationItem.title = "Baby"
        self.lblBabyName.text =  babyInfo["babyName"]!
        self.lblParentName.text = babyInfo["parentName"]!.uppercased()
        let spiltArray = babyInfo["babyDate"]?.components(separatedBy: "/")
        self.lblDate.text = spiltArray![0] + "." + spiltArray![1] + "." + spiltArray![2]
        self.lblBornTime.text = babyInfo["babyTime"]!
        self.lblBornTimeZone.text = babyInfo["babyTimeZone"]!
        self.lblweight.text = babyInfo["babyWeight"]!
        self.lblWeightType.text = babyInfo["babyWieghtType"]!
        self.lblHeight.text = babyInfo["babyHeight"]!
        self.lblHeightType.text = babyInfo["babyHeightType"]!
    }
    @IBAction func actionShare(_ sender: Any) {
        let image: UIImage? = screenShotVw.takeScreenshot()
        if image != nil {
            let imageToShare = [ image! ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert!", message: "Please Take or Choose photo", controller: self)
        }
    }
}
