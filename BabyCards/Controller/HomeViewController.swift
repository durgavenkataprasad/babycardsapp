//
//  ViewController.swift
//  BabyCards
//
//  Created by Praveenkumar R on 09/07/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet var collectionViewFilter: UICollectionView!
    @IBOutlet var collectionViewList: UICollectionView!
    var arrayFilterName = ["BOY","GIRL","FREE"]
    var arraySelectedFilterName = [String]()
 
    var arrPricesBoysTemplates = ["$0.99","$0.99","$0.99","$0.99","FREE","$0.99","$0.99","$0.99","$0.99","FREE","$0.99"]
    var arrPricesGirlsTemplates = ["$ 0.99","$ 0.99","$ 0.99","$ 0.99","FREE","$ 0.99","$ 0.99","$ 0.99","$0.99","FREE","$0.99"]
    
    var arrBoysProductID = ["ba_c001","ba_c002","ba_c003","ba_c004","","ba_c005","ba_c006","ba_c007","ba_c008","","ba_c009"]
 
    var arrGirlsProductID = ["ba_c0010","ba_c0011","ba_c0012","ba_c0013","","ba_c0014","ba_c0015","ba_c0016","ba_c0017","","ba_c0018"]
    
    var previewBoyImages = ["BabyB1","Baby2Boy","BabyBoy3","Baby_Boy4","Baby5B_1","Baby6B","Baby7B","Baby_Boy8","Baby9B","BabyTxt1B","BabyTxt2B"]
    var previewGirlsImages = ["Baby1Girl","Baby2Girl","Baby3Girl","Baby4Girl","Baby5G_1","Baby6G","Baby7G","Baby8G","Baby9G","BabyTxt1G","BabyTxt2G"]
    var preivewFreeImages = ["Baby_Boy5","Baby9B","Baby_Girl5","Baby9G"]
    var arrayBoyImages = ["BabyBoy1","Baby_Boy2","BabyBoy3","Baby4Boy","Baby5Boy","Baby6Boy","Baby7Boy","Baby8Boy","Baby9Boy","BabyText1Boy","BabyText2"]
    var arrayGirlsImages = ["Baby1Girl","Baby_Girl2","Baby3Girl","Baby4Girl","Baby5Girl","Baby6Girl","Baby7Girl","Baby8Girl","Baby9Girl","BabyText1Girl","BabyText2Girl"]
    var arrayTwinsImages = ["21","22","23","24","25","26","27","28","29","30","30"]
    var arrayFreeImages = ["Baby5Boy","Baby9Boy",
                           "Baby5Girl","Baby9Girl"]
    var shuffelImages = [[String:Any]]()
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        shuffelImages = Utilities.sharedInstance.shuffledInputArray().shuffled()
        self.setNavigationTittleImage()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func setNavigationTittleImage(){
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 60, height: 27))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "baby")
        imageView.image = image
        self.navigationItem.titleView = imageView
    }
    
    func routeToBoysTemplatesViewControllers(indexPath:IndexPath){
        // isHide (Used to hide parenet name in card details page)
        // vcName (Used to store view controller and route from card details page)
        
        switch indexPath.row {
        case 0:
            Utilities.sharedInstance.kDefaults.set("BabyBoyOneViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(false, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        case 1:
            Utilities.sharedInstance.kDefaults.set("BabyBoyTwoViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        case 2:
            Utilities.sharedInstance.kDefaults.set("BabyBoyThreeViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(false, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        case 3:
            Utilities.sharedInstance.kDefaults.set("BabyBoyFourViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        case 4:
            Utilities.sharedInstance.kDefaults.set("BabyBoyFiveViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        case 5:
            Utilities.sharedInstance.kDefaults.set("BabyBoySixViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        case 6:
            Utilities.sharedInstance.kDefaults.set("BabyBoySevenViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(false, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        case 7:
            Utilities.sharedInstance.kDefaults.set("BabyBoyEightViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        case 8:
            Utilities.sharedInstance.kDefaults.set("BabyBoyNineViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        case 9:
            Utilities.sharedInstance.kDefaults.set("BabyBoyTextOneViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(false, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        case 10:
            Utilities.sharedInstance.kDefaults.set("BabyBoyTextTwoViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        default:
            Utilities.displayFailureAlertWithMessage(title: "Alert", message: "Development in-progress", controller: self)
        }
    }
    func reloadCollectionView()
    {
        DispatchQueue.main.async {
            self.collectionViewList.reloadData()
        }
    }
    func routeToGirlsTemplatesViewControllers(indexPath:IndexPath){
        switch indexPath.row {
        case 0:
            Utilities.sharedInstance.kDefaults.set("BabyGirlOneViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(false, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        case 1:
            Utilities.sharedInstance.kDefaults.set("BabyGirlTwoViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        case 2:
            Utilities.sharedInstance.kDefaults.set("BabyGirlThreeViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(false, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        case 3:
            Utilities.sharedInstance.kDefaults.set("BabyGirlFourViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        case 4:
            Utilities.sharedInstance.kDefaults.set("BabyGirlFiveViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        case 5:
            Utilities.sharedInstance.kDefaults.set("BabyGirlSixViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        case 6:
            Utilities.sharedInstance.kDefaults.set("BabyGirlSevenViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(false, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        case 7:
            Utilities.sharedInstance.kDefaults.set("BabyGirlEightViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        case 8:
            Utilities.sharedInstance.kDefaults.set("BabyGirlNineViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        case 9:
            Utilities.sharedInstance.kDefaults.set("BabyGirlTextOneViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(false, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        case 10:
            Utilities.sharedInstance.kDefaults.set("BabyGirlTextTwoViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexPath)
        default:
            Utilities.displayFailureAlertWithMessage(title: "Alert", message: "This feature will be available next milestone", controller: self)
        }
    }
    
    func routeToTemplatesWithDefaultCategory(vcName:String,indexP:IndexPath){
        switch vcName {
            
        case "BabyBoyOneViewController":
            Utilities.sharedInstance.kDefaults.set("BabyBoyOneViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(false, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
            
        case "BabyBoyTwoViewController":
            Utilities.sharedInstance.kDefaults.set("BabyBoyTwoViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
            
        case "BabyBoyThreeViewController":
            Utilities.sharedInstance.kDefaults.set("BabyBoyThreeViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(false, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
            
        case "BabyBoyFourViewController":
            Utilities.sharedInstance.kDefaults.set("BabyBoyFourViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
            
        case "BabyBoyFiveViewController":
            Utilities.sharedInstance.kDefaults.set("BabyBoyFiveViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
            
        case "BabyBoySixViewController":
            Utilities.sharedInstance.kDefaults.set("BabyBoySixViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
            
        case "BabyBoySevenViewController":
            Utilities.sharedInstance.kDefaults.set("BabyBoySevenViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(false, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
            
        case "BabyBoyEightViewController":
            Utilities.sharedInstance.kDefaults.set("BabyBoyEightViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
            
        case "BabyBoyNineViewController":
            Utilities.sharedInstance.kDefaults.set("BabyBoyNineViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
            
        case "BabyBoyTextOneViewController":
            Utilities.sharedInstance.kDefaults.set("BabyBoyTextOneViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(false, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
            
        case "BabyBoyTextTwoViewController":
            Utilities.sharedInstance.kDefaults.set("BabyBoyTextTwoViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
            
        case "BabyGirlOneViewController":
            Utilities.sharedInstance.kDefaults.set("BabyGirlOneViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(false, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
            
        case "BabyGirlTwoViewController":
            Utilities.sharedInstance.kDefaults.set("BabyGirlTwoViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
            
        case "BabyGirlThreeViewController":
            Utilities.sharedInstance.kDefaults.set("BabyGirlThreeViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(false, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
            
        case "BabyGirlFourViewController":
            Utilities.sharedInstance.kDefaults.set("BabyGirlFourViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
            
        case "BabyGirlFiveViewController":
            Utilities.sharedInstance.kDefaults.set("BabyGirlFiveViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
            
        case "BabyGirlSixViewController":
            Utilities.sharedInstance.kDefaults.set("BabyGirlSixViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
            
        case "BabyGirlSevenViewController":
            Utilities.sharedInstance.kDefaults.set("BabyGirlSevenViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(false, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
            
        case "BabyGirlEightViewController":
            Utilities.sharedInstance.kDefaults.set("BabyGirlEightViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
            
        case "BabyGirlNineViewController":
            Utilities.sharedInstance.kDefaults.set("BabyGirlNineViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
            
        case "BabyGirlTextOneViewController":
            Utilities.sharedInstance.kDefaults.set("BabyGirlTextOneViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(false, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
            
        case "BabyGirlTextTwoViewController":
            Utilities.sharedInstance.kDefaults.set("BabyGirlTextTwoViewController", forKey: "vcName")
            Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
            Utilities.sharedInstance.kDefaults.synchronize()
            performSegue(withIdentifier: "Preview", sender: indexP)
        default:
            Utilities.displayFailureAlertWithMessage(title: "Alert", message: "This feature will be available next milestone", controller: self)
        }
    }
    @objc func cellDidTapped(indexPath: IndexPath){
        
        if arraySelectedFilterName.count == 0{
            
            let shuffleDictionary = shuffelImages[indexPath.row]
            let category = shuffleDictionary["category"]
            let viewController = shuffleDictionary["vcName"]
            
            Utilities.sharedInstance.kDefaults.set("OTHER", forKey: "category")
            Utilities.sharedInstance.kDefaults.synchronize()
            self.routeToTemplatesWithDefaultCategory(vcName: viewController as! String, indexP: indexPath)
            
        }else{
            if arraySelectedFilterName.contains("BOY"){
                self.routeToBoysTemplatesViewControllers(indexPath: indexPath)
            }
            else if (arraySelectedFilterName.contains("GIRL")){
                self.routeToGirlsTemplatesViewControllers(indexPath: indexPath)
            }
            else if (arraySelectedFilterName.contains("FREE")){
                
                if indexPath.row == 0{
                    Utilities.sharedInstance.kDefaults.set("BabyBoyFiveViewController", forKey: "vcName")
                    Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
                    Utilities.sharedInstance.kDefaults.synchronize()
                    performSegue(withIdentifier: "Preview", sender: indexPath)
                }else if(indexPath.row == 1){
                    Utilities.sharedInstance.kDefaults.set("BabyBoyNineViewController", forKey: "vcName")
                    Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
                    Utilities.sharedInstance.kDefaults.synchronize()
                    performSegue(withIdentifier: "Preview", sender: indexPath)
                }else if (indexPath.row == 2){
                    Utilities.sharedInstance.kDefaults.set("BabyGirlFiveViewController", forKey: "vcName")
                    Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
                    Utilities.sharedInstance.kDefaults.synchronize()
                    performSegue(withIdentifier: "Preview", sender: indexPath)
                }
                else{
                    Utilities.sharedInstance.kDefaults.set("BabyGirlNineViewController", forKey: "vcName")
                    Utilities.sharedInstance.kDefaults.set(true, forKey: "isHide")
                    Utilities.sharedInstance.kDefaults.synchronize()
                    performSegue(withIdentifier: "Preview", sender: indexPath)
                }
            }
            else{
                Utilities.displayFailureAlertWithMessage(title: "Alert", message: "Development in-progress", controller: self)
            }
        }
    }
}
//MARK: - UICollectionViewDelegate,DataSource Methods
extension HomeViewController : UICollectionViewDelegate,UICollectionViewDataSource,collectionViewReloadDelegate,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if collectionView == collectionViewFilter{
            return 1
        }else{
            var numOfSections: Int = 0
            if(arraySelectedFilterName.contains("TWINS")){
                let labelEmpty: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: collectionView.bounds.size.width, height: collectionView.bounds.size.height))
                labelEmpty.text          = "No templates found"
                labelEmpty.textColor     = UIColor.black
                labelEmpty.textAlignment = .center
                collectionView.backgroundView  = labelEmpty
            }
            else{
                numOfSections            = 1
                collectionView.backgroundView = nil
            }
            return numOfSections
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewFilter {
            return arrayFilterName.count
        }else{
            if (arraySelectedFilterName.contains("BOY")){
                return 11
            }
            else if(arraySelectedFilterName.contains("GIRL")){
                return 11
            }else if (arraySelectedFilterName.contains("FREE")){
                return 4
            }else if (arraySelectedFilterName.contains("TWINS")){
                return 0
            }else{
                return 22
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewFilter {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCell", for: indexPath) as! CustomCollectionViewCell
            cell.labelFilterName.text = arrayFilterName[indexPath.row]
            
            if indexPath.row == 0 {
                cell.viewContentView.backgroundColor = Constants.getCustomBoyColor()
            }else if indexPath.row == 1{
                cell.viewContentView.backgroundColor = Constants.getCustomGirlColor()
            }else if indexPath.row == 2{
                cell.viewContentView.backgroundColor = Constants.getCustomTwinsColor()
            }else if indexPath.row == 3{
                cell.viewContentView.backgroundColor = Constants.getCustomFreeColor()
            }
            let filtername = cell.labelFilterName.text
            if let cellfiltername = filtername{
                if arraySelectedFilterName.contains(cellfiltername){
                    cell.viewContentView.layer.cornerRadius = 10
                    cell.viewContentView.layer.borderColor = UIColor.white.cgColor
                    cell.viewContentView.layer.borderWidth = 3
                    cell.layer.shadowRadius = 3
                    cell.layer.shadowColor = UIColor.black.cgColor
                    cell.layer.shadowOpacity = 0.2
                    cell.layer.shadowOffset = CGSize(width: -1, height: 1)
                }else{
                    cell.viewContentView.layer.cornerRadius = 10
                    cell.viewContentView.layer.borderColor = UIColor.clear.cgColor
                    cell.viewContentView.layer.borderWidth = 3
                    cell.layer.shadowColor = UIColor.clear.cgColor
                }
            }
            return cell
        }else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListCell", for: indexPath) as! CustomCollectionViewCell
            cell.layer.cornerRadius = 10.0
            cell.layer.shadowColor = UIColor.gray.cgColor
            cell.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            cell.layer.shadowRadius = 2.0
            cell.layer.shadowOpacity = 0.2
            
            if (arraySelectedFilterName.contains("BOY")){
                switch indexPath.row {
                case 0:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY1_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesBoysTemplates[indexPath.row]
                    }
                case 1:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY2_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesBoysTemplates[indexPath.row]
                    }
                case 2:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY3_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesBoysTemplates[indexPath.row]
                    }
                case 3:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY4_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesBoysTemplates[indexPath.row]
                    }
                case 4:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY5_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesBoysTemplates[indexPath.row]
                    }
                case 5:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY6_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesBoysTemplates[indexPath.row]
                    }
                case 6:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY7_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesBoysTemplates[indexPath.row]
                    }
                case 7:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY8_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesBoysTemplates[indexPath.row]
                    }
                case 8:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY9_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesBoysTemplates[indexPath.row]
                    }
                case 9:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY10_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesBoysTemplates[indexPath.row]
                    }
                case 10:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY11_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesBoysTemplates[indexPath.row]
                    }
                default:
                    print("")
                }
            }else if(arraySelectedFilterName.contains("GIRL")){
                switch indexPath.row {
                case 0:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL1_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesBoysTemplates[indexPath.row]
                    }
                case 1:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL2_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesGirlsTemplates[indexPath.row]
                    }
                case 2:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL3_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesGirlsTemplates[indexPath.row]
                    }
                case 3:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL4_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesGirlsTemplates[indexPath.row]
                    }
                case 4:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL5_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesGirlsTemplates[indexPath.row]
                    }
                case 5:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL6_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesGirlsTemplates[indexPath.row]
                    }
                case 6:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL7_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesGirlsTemplates[indexPath.row]
                    }
                case 7:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL8_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesGirlsTemplates[indexPath.row]
                    }
                case 8:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL9_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesGirlsTemplates[indexPath.row]
                    }
                case 9:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL10_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesGirlsTemplates[indexPath.row]
                    }
                case 10:
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL11_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = arrPricesGirlsTemplates[indexPath.row]
                    }
                default:
                    print("")
                }
            }
            else if(arraySelectedFilterName.contains("TWINS")){
                cell.lblPrice.text = ""
            }
            else if(arraySelectedFilterName.contains("FREE")){
                cell.lblPrice.text = "FREE"
            }
            else{
                let shuffleDictionary = shuffelImages[indexPath.row]
                let templateID = (shuffleDictionary["isPurchased"] as! String)
                
                switch templateID {
                    
                case "BOY1_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY1_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                case "BOY2_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY2_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                case "BOY3_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY3_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                case "BOY4_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY4_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                case "BOY5_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY5_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                case "BOY6_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY6_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                case "BOY7_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY7_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                case "BOY8_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY8_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                case "BOY9_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY9_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                case "BOY10_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY10_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                case "BOY11_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "BOY11_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                    
                case "GIRL1_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL1_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                case "GIRL2_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL2_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                case "GIRL3_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL3_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                case "GIRL4_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL4_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                case "GIRL5_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL5_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                case "GIRL6_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL6_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                case "GIRL7_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL7_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                case "GIRL8_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL8_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                case "GIRL9_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL9_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                case "GIRL10_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL10_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                case "GIRL11_PURCHASED":
                    if (Utilities.sharedInstance.kDefaults.object(forKey: "GIRL11_PURCHASED") != nil) {
                        cell.lblPrice.text = ""
                    }else{
                        cell.lblPrice.text = (shuffleDictionary["price"] as! String)
                    }
                default:
                    print("")
                }
            }
            if arraySelectedFilterName.contains("BOY"){
                cell.imgVw.image = UIImage(named:arrayBoyImages[indexPath.row])
            }else if (arraySelectedFilterName.contains("GIRL")){
                cell.imgVw.image = UIImage(named:arrayGirlsImages[indexPath.row])
            }else if (arraySelectedFilterName.contains("TWINS")){
                cell.imgVw.image = UIImage(named:arrayTwinsImages[indexPath.row])
            }else if (arraySelectedFilterName.contains("FREE")){
                cell.imgVw.image = UIImage(named:arrayFreeImages[indexPath.row])
            }
            else{
                let shuffleDictionary = shuffelImages[indexPath.row]
                cell.imgVw.image = UIImage(named:shuffleDictionary["image"] as! String)
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewFilter {
            let cell = collectionView.cellForItem(at: indexPath) as! CustomCollectionViewCell
            let filterName = cell.labelFilterName.text
            
            if arraySelectedFilterName.contains(filterName!){
                arraySelectedFilterName.removeAll()
                self.viewDidLoad()
            }else{
                if arraySelectedFilterName.count > 0{
                    arraySelectedFilterName.removeAll()
                }
                if let text = filterName{
                    if self.arraySelectedFilterName.contains(text){
                        self.arraySelectedFilterName.remove(at: Int(text)!)
                    }
                    else{
                        self.arraySelectedFilterName.append(text)
                        Utilities.sharedInstance.kDefaults.set(text, forKey: "category")
                        Utilities.sharedInstance.kDefaults.synchronize()
                    }
                }
            }
            collectionViewFilter.reloadData()
            collectionViewList.reloadData()
        }else{
            self.cellDidTapped(indexPath: indexPath)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionViewList {
            return CGSize(width:self.collectionViewList.bounds.size.width/2 - 10, height:198)
        }else{
            return CGSize(width: self.collectionViewFilter.bounds.size.width/3 - 8, height: 60)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "Preview") {
            guard let popupViewController = segue.destination as? PreviewViewController
                else { return }
            let cellIndexPath = sender as? IndexPath
            popupViewController.customBlurEffectStyle = .dark
            popupViewController.delegate = self
            popupViewController.customAnimationDuration = 0.7
            popupViewController.customInitialScaleAmmount = 0.1
            popupViewController.templateIndex = (cellIndexPath?.row)!
            
            if arraySelectedFilterName.count == 0{
                let shuffleDictionary = shuffelImages[(cellIndexPath?.row)!]
                popupViewController.previewImage = UIImage(named:shuffleDictionary["preview_image"] as! String)!
                popupViewController.templatePrice = (shuffleDictionary["price"] as! String)
                popupViewController.templateProductID = (shuffleDictionary["product_id"] as! String)
            }else{
                if arraySelectedFilterName.contains("BOY"){
                    popupViewController.previewImage =  UIImage(named:previewBoyImages[(cellIndexPath?.row)!])!
                    popupViewController.templatePrice = arrPricesBoysTemplates[(cellIndexPath?.row)!]
                    popupViewController.templateProductID = arrBoysProductID[(cellIndexPath?.row)!]
                }else if (arraySelectedFilterName.contains("GIRL")){
                    popupViewController.previewImage =  UIImage(named:previewGirlsImages[(cellIndexPath?.row)!])!
                    popupViewController.templatePrice = arrPricesGirlsTemplates[(cellIndexPath?.row)!]
                    popupViewController.templateProductID = arrGirlsProductID[(cellIndexPath?.row)!]

                }else if (arraySelectedFilterName.contains("TWINS")){
                    popupViewController.previewImage =  UIImage(named:arrayTwinsImages[(cellIndexPath?.row)!])!
                    popupViewController.templatePrice = "FREE"
                }
                else{
                    popupViewController.templatePrice = "FREE"
                    popupViewController.previewImage =  UIImage(named:preivewFreeImages[(cellIndexPath?.row)!])!
                }
            }
        }
    }
}
extension MutableCollection {
    mutating func shuffle() {
        let c = count
        guard c > 1 else { return }
        for (firstUnshuffled, unshuffledCount) in zip(indices, stride(from: c, to: 1, by: -1)) {
            let d: Int = numericCast(arc4random_uniform(numericCast(unshuffledCount)))
            let i = index(firstUnshuffled, offsetBy: d)
            swapAt(firstUnshuffled, i)
        }
    }
}
extension Sequence {
    func shuffled() -> [Element] {
        var result = Array(self)
        result.shuffle()
        return result
    }
}
