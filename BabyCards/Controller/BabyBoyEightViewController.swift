//
//  BabyBoyEightViewController.swift
//  BabyCards
//
//  Created by CIPL108-MOBILITY on 14/08/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class BabyBoyEightViewController: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var imgBaby: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblMeasuring: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var viewScreenShot: UIView!
    var babyInfo = [String:String]()
    var imagePicker = UIImagePickerController()
    @IBOutlet weak var scrollImg: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setScrollVwDelegate()
        self.DispalyBabyInfo()
    }
    //MARK: - PRASAD
    func DispalyBabyInfo(){
        self.navigationItem.title = "Baby"
        self.lblName.text =  babyInfo["babyName"]!.uppercased()
        let formattedDate = Utilities.formattedDateFromString(dateString:babyInfo["babyDate"]!, withFormat: "MMM dd yyyy")
        let spiltArray = formattedDate?.components(separatedBy: " ")
        self.lblDate.text = spiltArray![1] + " " + spiltArray![0].uppercased() + " " + spiltArray![2]
        lblTime.text = babyInfo["babyTime"]! + babyInfo["babyTimeZone"]!
        
        let tintColor = UIColor(red:128/255.0, green:222/255.0 ,blue:228/255.0 , alpha:1)
        let weightString = "WEIGHING \(babyInfo["babyWeight"]! + babyInfo["babyWieghtType"]!)"
        let string_to_color = "\(babyInfo["babyWeight"]! + babyInfo["babyWieghtType"]!)"
        let range = (weightString as NSString).range(of: string_to_color)
        let attribute = NSMutableAttributedString.init(string: weightString)
        attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: tintColor , range: range)
        lblWeight.attributedText = attribute
        
        let heightString = "WEIGHING \(babyInfo["babyHeight"]! + babyInfo["babyHeightType"]!)"
        let heightColorString = "\(babyInfo["babyHeight"]! + babyInfo["babyHeightType"]!)"
        let heightColorRange = (heightString as NSString).range(of: heightColorString)
        let heightAttribute = NSMutableAttributedString.init(string: heightString)
        heightAttribute.addAttribute(NSAttributedStringKey.foregroundColor, value: tintColor , range: heightColorRange)
        lblMeasuring.attributedText = heightAttribute
     }
    @IBAction func actionShare(_ sender: Any) {
        let image:UIImage? = viewScreenShot.takeScreenshot()
        if image != nil {
            let imageToShare = [ image! ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert!", message: "Please Take or Choose photo", controller: self)
        }
    }
    @IBAction func actionTakePhoto(_ sender: Any) {
        openCamera()
    }
    @IBAction func actionPhotoLibrary(_ sender: Any) {
        openGallary()
    }
    func setScrollVwDelegate(){
        scrollImg.delegate = self
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgBaby
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension BabyBoyEightViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    //MARK: - OpenCamera
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    //MARK: - OpenGallary
    func openGallary(){
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    //MARK: -UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        //You will get cropped image here..
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.imgBaby.image = image
        }else{
            print("Something went wrong")
        }
    }
}

