//
//  BabyGirlFourViewController.swift
//  BabyCards
//
//  Created by CIPL108-MOBILITY on 23/08/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class BabyGirlFourViewController: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var imgViewGirl4: UIImageView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var screenShotView: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var scrollImg: UIScrollView!
    var girl4Info = [String:String]()
    var imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dispalyGirl4Info()
        self.dispalyGirl4Info()
        self.setScrollVwDelegate()
    }
    func setScrollVwDelegate(){
        scrollImg.delegate = self
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgViewGirl4
    }
    func dispalyGirl4Info(){
        self.navigationItem.title = "Baby"
        self.lblName.text =  girl4Info["babyName"]!
        let formattedDate = Utilities.formattedDateFromString(dateString:girl4Info["babyDate"]!, withFormat: "MMM dd yyyy")
        let splitArray = formattedDate?.components(separatedBy: " ")
        self.lblDate.text = splitArray![0] + " " + splitArray![1] + " " + splitArray![2]
        self.lblTime.text = girl4Info["babyTime"]! + girl4Info["babyTimeZone"]!
        if girl4Info["babyWeight"] == "KGS"{
            self.lblWeight.text = girl4Info["babyWeight"]! + " " + "POUNDS"
        }else{
            self.lblWeight.text = girl4Info["babyWeight"]! + " " + "KILOGRAMS"
        }
        if girl4Info["babyWeight"] == "CM"{
            self.lblHeight.text = girl4Info["babyHeight"]! + " " + "INCHES"
        }else{
            self.lblHeight.text = girl4Info["babyHeight"]! + " " + "CENTREMETRES"
        }
    }
    @IBAction func actionShare(_ sender: Any) {
        let image: UIImage? = screenShotView.takeScreenshot()
        if image != nil {
            let imageToShare = [ image! ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert!", message: "Please Take or Choose photo", controller: self)
        }
    }
    @IBAction func actionTakePhoto(_ sender: Any) {
        openCamera()
    }
    @IBAction func actionPhotoLibrary(_ sender: Any) {
        openGallary()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension BabyGirlFourViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    //MARK: - OpenCamera
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    //MARK: - OpenGallary
    func openGallary(){
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    //MARK: -UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        //You will get cropped image here..
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.imgViewGirl4.image = image
        }else{
            print("Something went wrong")
        }
    }
}
