//
//  BabyGirlSevenViewController.swift
//  BabyCards
//
//  Created by CIPL108-MOBILITY on 23/08/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class BabyGirlSevenViewController: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var screenShotView: UIView!
    @IBOutlet weak var scrollImg: UIScrollView!
    @IBOutlet weak var imgBabySeven: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblParentName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblTimeZone: UILabel!
    @IBOutlet weak var lblWeightType: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblHeightType: UILabel!
    var girlSevenInfo = [String:String]()
    var imagePicker = UIImagePickerController()
    @IBOutlet weak var viewBoarder: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBoarder.layer.borderWidth = 3.0
        viewBoarder.layer.borderColor = UIColor (red: 255.0/255.0, green: 133.0/255.0, blue: 151.0/255.0, alpha: 1.0).cgColor
        self.setScrollVwDelegate()
        self.dispalyGirl7Info()
    }
    func setScrollVwDelegate(){
        scrollImg.delegate = self
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
    }
    func dispalyGirl7Info(){
        self.navigationItem.title = "Baby"
        self.lblName.text =  girlSevenInfo["babyName"]!
        self.lblParentName.text = girlSevenInfo["parentName"]?.uppercased()
        
        let splitArray = girlSevenInfo["babyDate"]?.components(separatedBy: "/")
        self.lblDate.text = splitArray![0] + "." + splitArray![1] + "." +  splitArray![2]
        self.lblTime.text = girlSevenInfo["babyTime"]!
        self.lblTimeZone.text = girlSevenInfo["babyTimeZone"]!
        self.lblWeight.text = girlSevenInfo["babyWeight"]!
        self.lblWeightType.text = girlSevenInfo["babyWieghtType"]!
        self.lblHeight.text = girlSevenInfo["babyHeight"]!
        self.lblHeightType.text = girlSevenInfo["babyHeightType"]!
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgBabySeven
    }
    @IBAction func actionShare(_ sender: Any) {
        let image: UIImage? = screenShotView.takeScreenshot()
        if image != nil {
            let imageToShare = [ image! ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert!", message: "Please Take or Choose photo", controller: self)
        }
    }
    @IBAction func actionPhotoLibrary(_ sender: Any) {
        openGallary()
    }
    @IBAction func actionTakePhoto(_ sender: Any) {
        openCamera()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension BabyGirlSevenViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    //MARK: - OpenCamera
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    //MARK: - OpenGallary
    func openGallary(){
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    //MARK: -UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        //You will get cropped image here..
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.imgBabySeven.image = image
        }else{
            print("Something went wrong")
        }
    }
}
