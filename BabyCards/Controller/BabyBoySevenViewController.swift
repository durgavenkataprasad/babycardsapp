//
//  BabyBoySevenViewController.swift
//  BabyCards
//
//  Created by CIPL108-MOBILITY on 14/08/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class BabyBoySevenViewController: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var imgBoy: UIImageView!
    @IBOutlet weak var viewBorder: UIView!
    @IBOutlet weak var screenShotView: UIView!
    @IBOutlet weak var lblBabyName: UILabel!
    @IBOutlet weak var lblParentName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTimeZone: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblWeightType: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblHeightType: UILabel!
    var imagePicker = UIImagePickerController()
    var babyInfo = [String:String]()
    @IBOutlet weak var scrollImg: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBorder.layer.borderWidth = 3.0
        viewBorder.layer.borderColor = UIColor (red: 122.0/255.0, green: 222.0/255.0, blue: 228.0/255.0, alpha: 1.0).cgColor
        self.setScrollVwDelegate()
        self.DispalyBabyInfo()
    }
    
    func DispalyBabyInfo(){
        self.navigationItem.title = "Baby"
        self.lblBabyName.text =  babyInfo["babyName"]!
        self.lblParentName.text = babyInfo["parentName"]?.uppercased()
        
        let spiltArray = babyInfo["babyDate"]?.components(separatedBy: "/")
        self.lblDate.text = spiltArray![0] + "." + spiltArray![1] + "." +  spiltArray![2]
        self.lblTime.text = babyInfo["babyTime"]!
        self.lblTimeZone.text = babyInfo["babyTimeZone"]!
        self.lblWeight.text = babyInfo["babyWeight"]!
        self.lblWeightType.text = babyInfo["babyWieghtType"]!
        self.lblHeight.text = babyInfo["babyHeight"]!
        self.lblHeightType.text = babyInfo["babyHeightType"]!
    }
    @IBAction func actionTakePhoto(_ sender: Any) {
        openCamera()
    }
    @IBAction func actionLibrary(_ sender: Any) {
        openGallary()
    }
    func setScrollVwDelegate(){
        scrollImg.delegate = self
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgBoy
    }
    @IBAction func actionShare(_ sender: Any) {
        let image: UIImage? = screenShotView.takeScreenshot()
        if image != nil {
            let imageToShare = [ image! ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert!", message: "Please Take or Choose photo", controller: self)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension BabyBoySevenViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    //MARK: - OpenCamera
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    //MARK: - OpenGallary
    func openGallary(){
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    //MARK: -UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        //You will get cropped image here..
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.imgBoy.image = image
        }else{
            print("Something went wrong")
        }
    }
}

