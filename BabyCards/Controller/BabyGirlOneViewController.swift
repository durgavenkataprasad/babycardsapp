//
//  BabyGirlOneViewController.swift
//  BabyCards
//
//  Created by CIPL108-MOBILITY on 23/08/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class BabyGirlOneViewController: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var lblBabyName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var heightView: UIView!
    @IBOutlet weak var lblParentsName: UILabel!
    @IBOutlet weak var calenderView: UIView!
    @IBOutlet weak var lblMonthName: UILabel!
    @IBOutlet weak var lblCalenderDate: UILabel!
    @IBOutlet weak var lblCalenderYear: YAxisLabel!
    @IBOutlet weak var viewCalenderPin1: UIView!
    @IBOutlet weak var viewCalenderPin2: UIView!
    @IBOutlet weak var grilImgView: UIImageView!
    var girl1Info = [String:String]()
    var imagePicker = UIImagePickerController()
    @IBOutlet weak var screenShotView: UIView!
    @IBOutlet weak var scrollImg: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setScrollVwDelegate()
        navigationItem.title = "Baby"
        self.displayGirl1Info()
        self.setBoarderColorCalender()
        self.roatateViewWithAngle()
        self.setBabyBornDateInCalender()
    }
    func setScrollVwDelegate(){
        scrollImg.delegate = self
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.grilImgView
    }
    func roatateViewWithAngle(){
        self.heightView.transform = CGAffineTransform(rotationAngle: 0.2)
        self.calenderView.transform = CGAffineTransform(rotationAngle: -0.2)
        self.viewCalenderPin1.transform = CGAffineTransform(rotationAngle: -0.02)
        self.viewCalenderPin2.transform = CGAffineTransform(rotationAngle: -0.02)
    }
    func displayGirl1Info(){
        self.lblBabyName.text = girl1Info["babyName"] ?? ""
        self.lblParentsName.text = girl1Info["parentName"] ?? ""
        self.lblTime.attributedText = Utilities.setAttributeStringWithSize(tittle: girl1Info["babyTime"]!, subTittle: girl1Info["babyTimeZone"]!, attributeTittleSize: 35, attributeSubtittleSize: 13, attributeColor: Constants.GirlOneTemplateTextTintColor())
        self.lblHeight.attributedText = Utilities.setAttributeStringWithSize(tittle: girl1Info["babyHeight"]!, subTittle: girl1Info["babyHeightType"]!, attributeTittleSize: 45, attributeSubtittleSize: 13, attributeColor: Constants.babyHeightMeasureColor())
        self.lblWeight.attributedText = Utilities.setAttributeStringWithSize(tittle: girl1Info["babyWeight"]!, subTittle: girl1Info["babyWieghtType"]!, attributeTittleSize: 35, attributeSubtittleSize: 13, attributeColor: Constants.GirlOneTemplateTextTintColor())
    }
    func setBabyBornDateInCalender(){
        let selectedDate = girl1Info["babyDate"] ?? ""
        let array = selectedDate.components(separatedBy: "/")
        self.lblCalenderDate.text = array[0]
        let monthNumber = Int(array[1])
        let fmt = DateFormatter()
        fmt.dateFormat = "MM"
        let month = fmt.monthSymbols[monthNumber! - 1]
        self.lblCalenderYear.text = array[2]
        self.lblMonthName.text = month.uppercased()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func setBoarderColorCalender(){
        viewCalenderPin1.layer.borderWidth = 2
        viewCalenderPin1.layer.borderColor = UIColor.white.cgColor
        viewCalenderPin1.layer.cornerRadius = 3
        viewCalenderPin1.layer.masksToBounds = true
        viewCalenderPin2.layer.borderWidth = 2
        viewCalenderPin2.layer.borderColor = UIColor.white.cgColor
        viewCalenderPin2.layer.cornerRadius = 3
        viewCalenderPin2.layer.masksToBounds = true
        lblCalenderYear.adjustsFontSizeToFitWidth = true
    }
    @IBAction func actionTakePhoto(_ sender: Any) {
        openCamera()
    }
    @IBAction func actionShare(_ sender: Any) {
        let image: UIImage? = screenShotView.takeScreenshot()
        if image != nil {
            let imageToShare = [image!]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert!", message: "Please Take or Choose photo", controller: self)
        }
    }
    @IBAction func actionPhotoLibrary(_ sender: Any) {
        openGallary()
     }
}
extension BabyGirlOneViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
            
        }else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallary(){
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        //You will get cropped image here..
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.grilImgView.image = image
        }else{
            print("Something went wrong")
        }
    }
}
