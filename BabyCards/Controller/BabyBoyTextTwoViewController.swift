//
//  BabyBoyTextTwoViewController.swift
//  BabyCards
//
//  Created by CIPL108-MOBILITY on 13/08/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class BabyBoyTextTwoViewController: UIViewController {
   
    @IBOutlet weak var lblBabyName: UILabel!
    @IBOutlet weak var lblBornDate: UILabel!
    @IBOutlet weak var lblBornTime: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblweight: UILabel!
    @IBOutlet weak var lblParentName: UILabel!
    var babyInfo = [String:String]()
    @IBOutlet weak var screenShotVw: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.DisplayBabyInfo()
     }
    @IBAction func actionShare(_ sender: Any) {
        let image:UIImage? = screenShotVw.takeScreenshot()
        if image != nil {
            let imageToShare = [ image! ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert!", message: "Please Take or Choose photo", controller: self)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func DisplayBabyInfo(){
        self.navigationItem.title = "Baby"
        
        self.lblBabyName.text =  babyInfo["babyName"]!
        
        let formattedDate = Utilities.formattedDateFromString(dateString:babyInfo["babyDate"]!, withFormat: "dd MMM yyyy")
        let spiltArray = formattedDate?.components(separatedBy: " ")
        self.lblBornDate.text = spiltArray![0] + " " + spiltArray![1].uppercased() + " " + spiltArray![2]
          self.lblBornTime.text = babyInfo["babyTime"]! + "\n" + babyInfo["babyTimeZone"]!
        lblBabyName.text = babyInfo["babyName"]!.uppercased()
        self.lblweight.text = babyInfo["babyWeight"]! + "\n" + babyInfo["babyWieghtType"]!
        self.lblHeight.text = babyInfo["babyHeight"]! + "\n" + babyInfo["babyHeightType"]!
    }
}
