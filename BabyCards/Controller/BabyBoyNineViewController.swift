//
//  BabyBoyNineViewController.swift
//  BabyCards
//
//  Created by CIPL108-MOBILITY on 16/08/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class BabyBoyNineViewController: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var rotateBabyView: UIView!
    @IBOutlet weak var scrollImg: UIScrollView!
    @IBOutlet weak var imgViewBaby: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var screenShotView: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    var babyInfo = [String:String]()
    var imagePicker = UIImagePickerController()
    @IBOutlet weak var lblName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setScrollVwDelegate()
        self.DispalyBabyInfo()
        self.rotateBabyView.transform = CGAffineTransform(rotationAngle: 0.2)
    }
    func DispalyBabyInfo(){
        self.navigationItem.title = "Baby"
//        self.lblName.text =  babyInfo["babyName"]!
        let formattedDate = Utilities.formattedDateFromString(dateString:babyInfo["babyDate"]!, withFormat: "dd MMM yyyy")
        let spiltArray = formattedDate?.components(separatedBy: " ")
        self.lblDate.text = spiltArray![0] + " " + spiltArray![1].uppercased() + " " + spiltArray![2]
        self.lblTime.text = babyInfo["babyTime"]! + "\n\(babyInfo["babyTimeZone"]!)"
        lblWeight.text = babyInfo["babyWeight"]! + "\n\(babyInfo["babyWieghtType"]!)"
        lblHeight.text = babyInfo["babyHeight"]! + "\n\(babyInfo["babyHeightType"]!)"
    }
    func setScrollVwDelegate(){
        scrollImg.delegate = self
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgViewBaby
    }
    @IBAction func actionTakePhoto(_ sender: Any) {
        openCamera()
    }
    @IBAction func actionPhotoLibrary(_ sender: Any) {
        openGallary()
    }
    @IBAction func actionShare(_ sender: Any) {
        let image:UIImage? = screenShotView.takeScreenshot()
        if image != nil {
            let imageToShare = [ image! ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert!", message: "Please Take or Choose photo", controller: self)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension BabyBoyNineViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    //MARK: - OpenCamera
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    //MARK: - OpenGallary
    func openGallary(){
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    //MARK: -UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        //You will get cropped image here..
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.imgViewBaby.image = image
        }else{
            print("Something went wrong")
        }
    }
}
