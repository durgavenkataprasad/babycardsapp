//
//  BabyGirlNineViewController.swift
//  BabyCards
//
//  Created by CIPL108-MOBILITY on 23/08/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class BabyGirlNineViewController: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var scrollImg: UIScrollView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var screenShotView: UIView!
    @IBOutlet weak var imgGirlNine: UIImageView!
    @IBOutlet weak var rotateImgView: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    var imagePicker = UIImagePickerController()
    var girl9Info = [String:String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setScrollVwDelegate()
        self.dispaly9GirlInfo()
        self.rotateImgView.transform = CGAffineTransform(rotationAngle: 0.2)
    }
    func dispaly9GirlInfo(){
        self.navigationItem.title = "Baby"
        self.lblName.text =  girl9Info["babyName"]!
        let formattedDate = Utilities.formattedDateFromString(dateString:girl9Info["babyDate"]!, withFormat: "dd MMM yyyy")
        let splitArray = formattedDate?.components(separatedBy: " ")
        self.lblDate.text = splitArray![0] + " " + splitArray![1].uppercased() + " " + splitArray![2]
        self.lblTime.text = girl9Info["babyTime"]! + "\n\(girl9Info["babyTimeZone"]!)"
        lblWeight.text = girl9Info["babyWeight"]! + "\n\(girl9Info["babyWieghtType"]!)"
        lblHeight.text = girl9Info["babyHeight"]! + "\n\(girl9Info["babyHeightType"]!)"
    }
    func setScrollVwDelegate(){
        scrollImg.delegate = self
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgGirlNine
    }
    @IBAction func actionTakePhoto(_ sender: Any) {
        openCamera()
    }
    @IBAction func actionPhotoLibrary(_ sender: Any) {
        openGallary()
    }
    @IBAction func actionShare(_ sender: Any) {
        let image:UIImage? = screenShotView.takeScreenshot()
        if image != nil {
            let imageToShare = [ image! ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert!", message: "Please Take or Choose photo", controller: self)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension BabyGirlNineViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    //MARK: - OpenCamera
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    //MARK: - OpenGallary
    func openGallary(){
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    //MARK: -UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        //You will get cropped image here..
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.imgGirlNine.image = image
        }else{
            print("Something went wrong")
        }
    }
}
