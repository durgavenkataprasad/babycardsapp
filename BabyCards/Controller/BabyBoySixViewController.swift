//
//  BabyBoySixViewController.swift
//  BabyCards
//
//  Created by CIPL108-MOBILITY on 13/08/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class BabyBoySixViewController: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var lblBabyName: UILabel!
    @IBOutlet weak var lblBornDate: UILabel!
    @IBOutlet weak var lblBornTime: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var babyImgVw: UIImageView!
    @IBOutlet weak var viewOvalView : OvalView!
    var babyInfo = [String:String]()
    var imagePicker = UIImagePickerController()
    @IBOutlet weak var screenShotVw: UIView!
    @IBOutlet weak var scrollImg: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.DisplayBabyInfo()
        self.setScrollVwDelegate()
    }
    func setScrollVwDelegate(){
        scrollImg.delegate = self
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.babyImgVw
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func DisplayBabyInfo(){
        self.navigationItem.title = "Baby"
        self.lblBabyName.text =  babyInfo["babyName"]!
        let formattedDate = Utilities.formattedDateFromString(dateString:babyInfo["babyDate"]!, withFormat: "MMM dd yyyy")
        let spiltArray = formattedDate?.components(separatedBy: " ")
        lblBornDate.text = "\(spiltArray![0].uppercased())\n\(spiltArray![1])\n\(spiltArray![2])"
        lblHeight.attributedText = Utilities.setAttributeStringWithSize(tittle: babyInfo["babyHeight"]! + "\n", subTittle: babyInfo["babyHeightType"]!, attributeTittleSize: 28, attributeSubtittleSize: 13, attributeColor: UIColor.white)
        lblWeight.attributedText = Utilities.setAttributeStringWithSize(tittle: babyInfo["babyWeight"]! + "\n", subTittle: babyInfo["babyWieghtType"]!, attributeTittleSize: 28, attributeSubtittleSize: 13, attributeColor: UIColor.white)
        lblBornTime.attributedText = Utilities.setAttributeStringWithSize(tittle: babyInfo["babyTime"]! + "\n", subTittle: babyInfo["babyTimeZone"]!, attributeTittleSize: 30, attributeSubtittleSize: 14, attributeColor: UIColor.white)
      }
    @IBAction func actionShare(_ sender: Any) {
        let image: UIImage? = screenShotVw.takeScreenshot()
        if image != nil {
            let imageToShare = [ image! ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert!", message: "Please Take or Choose photo", controller: self)
        }
    }
    @IBAction func actionPhotoLibrary(_ sender: Any) {
        openGallary()
    }
    @IBAction func actionTakePhoto(_ sender: Any) {
        openCamera()
    }
}
extension BabyBoySixViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    //MARK: - OpenCamera
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
            
        }else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    //MARK: - OpenGallary
    func openGallary(){
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    //MARK: -UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        //You will get cropped image here..
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.babyImgVw.image = image
        }else{
            print("Something went wrong")
        }
    }
}
extension UIView {
    func takeScreenshot() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if (image != nil)
        {
            return image!
        }
        return UIImage()
    }
}
