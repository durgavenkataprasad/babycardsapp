//
//  BabyGirlFiveViewController.swift
//  BabyCards
//
//  Created by CIPL108-MOBILITY on 23/08/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class BabyGirlFiveViewController: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var screenShotView: UIView!
    @IBOutlet weak var imgGirlFive: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    var imagePicker = UIImagePickerController()
    var girl5Info = [String:String]()
    
    @IBOutlet weak var scrollImg: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dispalyGirlFiveInfo()
        self.setScrollVwDelegate()
    }
    func setScrollVwDelegate(){
        scrollImg.delegate = self
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgGirlFive
    }
    func dispalyGirlFiveInfo(){
        self.navigationItem.title = "Baby"
        self.lblName.text = girl5Info["babyName"]!
        
        let selectedDate = girl5Info["babyDate"] ?? ""
        let array = selectedDate.components(separatedBy: "/")
        let monthNumber = Int(array[1])
        let fmt = DateFormatter()
        fmt.dateFormat = "MM"
        let month = fmt.monthSymbols[monthNumber! - 1]
        
        self.lblDate.text =  month + " " + array[0] + " " + array[2]
        self.lblTime.attributedText = Utilities.setAttributeStringWithSizeWithCustomFont(tittle: girl5Info["babyTime"]!, subTittle: girl5Info["babyTimeZone"]!, attributeTittleSize: 20, attributeSubtittleSize: 13, attributeColor: UIColor.white, font: "JosefinSans-Bold")
        self.lblWeight.attributedText = Utilities.setAttributeStringWithSizeWithCustomFont(tittle: girl5Info["babyWeight"]!, subTittle: girl5Info["babyWieghtType"]!, attributeTittleSize: 20, attributeSubtittleSize: 13, attributeColor: UIColor.white, font: "JosefinSans-Bold")
        self.lblHeight.attributedText = Utilities.setAttributeStringWithSizeWithCustomFont(tittle: girl5Info["babyHeight"]!, subTittle: girl5Info["babyHeightType"]!, attributeTittleSize: 20, attributeSubtittleSize: 13, attributeColor: UIColor.white, font: "JosefinSans-Bold")
    }
    @IBAction func actionShare(_ sender: Any) {
        let image: UIImage? = screenShotView.takeScreenshot()
        if image != nil {
            let imageToShare = [ image! ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert!", message: "Please Take or Choose photo", controller: self)
        }
    }
    @IBAction func actionPhotoLibrary(_ sender: Any) {
        openGallary()
    }
    @IBAction func actionTakePhoto(_ sender: Any) {
        openCamera()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension BabyGirlFiveViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    //MARK: - OpenCamera
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    //MARK: - OpenGallary
    func openGallary(){
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    //MARK: -UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        //You will get cropped image here..
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.imgGirlFive.image = image
        }else{
            print("Something went wrong")
        }
    }
}
