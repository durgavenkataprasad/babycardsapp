//
//  PreviewViewController.swift
//  BabyCards
//
//  Created by Praveenkumar R on 09/07/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit
import StoreKit
import MBProgressHUD
//MARK: step 1 Add Protocol here
protocol collectionViewReloadDelegate: class {
    func reloadCollectionView()
}
class PreviewViewController: UIViewController,SKProductsRequestDelegate,
SKPaymentTransactionObserver,UIGestureRecognizerDelegate{
    
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var popupContentContainerView: UIView!
    weak var delegate: collectionViewReloadDelegate?
    @IBOutlet weak var btnBuy: UIButton!
    var customBlurEffectStyle: UIBlurEffectStyle!
    var customInitialScaleAmmount: CGFloat!
    var customAnimationDuration: TimeInterval!
    @IBOutlet weak var imgVw: UIImageView!
    var templatePrice = String()
    
    var previewImage = UIImage()
    var templateIndex = Int()
    // Product ID's
    
    let BABY_BOY_1 = "ba_c001"
    let BABY_BOY_2 = "ba_c002"
    let BABY_BOY_3 = "ba_c003"
    let BABY_BOY_4 = "ba_c004"
    let BABY_BOY_5 = ""
    let BABY_BOY_6 = "ba_c005"
    let BABY_BOY_7 = "ba_c006"
    let BABY_BOY_8 = "ba_c007"
    let BABY_BOY_9 = "ba_c008"
    let BABY_BOY_10 = ""
    let BABY_BOY_11 = "ba_c009"
    let BABY_GIRL_1 = "ba_c0010"
    let BABY_GIRL_2 = "ba_c0011"
    let BABY_GIRL_3 = "ba_c0012"
    let BABY_GIRL_4 = "ba_c0013"
    let BABY_GIRL_5 = ""
    let BABY_GIRL_6 = "ba_c0014"
    let BABY_GIRL_7 = "ba_c0015"
    let BABY_GIRL_8 = "ba_c0016"
    let BABY_GIRL_9 = "ba_c0017"
    let BABY_GIRL_10 = ""
    let BABY_GIRL_11 = "ba_c0018"
    
    var templateProductID : String?
    var productID = ""
    var productsRequest = SKProductsRequest()
    var iapProducts = [SKProduct]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if templatePrice != "FREE"{
//            fetchAvailableProducts()
        }
        if (previewImage == UIImage(named: "Baby2Boy") || previewImage == UIImage(named: "Baby2Girl")){
            let size = CGSize(width: UIScreen.main.bounds.size.width - 20, height: UIScreen.main.bounds.size.width - 20)
            imgVw.image = self.resizeImage(image: previewImage, targetSize: size)
        }else{
            let resizedImage = self.resizeImageWithAspect(image: previewImage, scaledToMaxWidth: self.view.frame.width - 20, maxHeight: self.view.frame.height-200)
            imgVw.image = resizedImage
        }
        lblPrice.text = templatePrice
        modalPresentationCapturesStatusBarAppearance = true
        self.isPurchasedProducts()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view == self.view {
            if touch?.view?.tag == 100{
                dismiss(animated: true)
            }
          }
    }
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    override func viewDidAppear(_ animated: Bool) {
    }
    
    
    func imageWithSize(image: UIImage,size: CGSize)->UIImage{
        if UIScreen.main.responds(to: #selector(NSDecimalNumberBehaviors.scale)){
            UIGraphicsBeginImageContextWithOptions(size,false,UIScreen.main.scale);
        }
        else
        {
            UIGraphicsBeginImageContext(size);
        }
        image.draw( in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        var newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage!;
    }
    //Summon this function VVV
    func resizeImageWithAspect(image: UIImage,scaledToMaxWidth width:CGFloat,maxHeight height :CGFloat)->UIImage
    {
        let oldWidth = image.size.width;
        let oldHeight = image.size.height;
        
        let scaleFactor = (oldWidth > oldHeight) ? width / oldWidth : height / oldHeight;
        let newHeight = oldHeight * scaleFactor;
        let newWidth = oldWidth * scaleFactor;
        let newSize = CGSize(width: self.view.frame.size.width - 20, height: newHeight)
        return imageWithSize(image: image, size: newSize);
    }
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading"
    }
    func isPurchasedProducts(){
        let vcName = Utilities.sharedInstance.kDefaults.object(forKey: "vcName") as! String
        switch vcName {
        case "BabyBoyOneViewController":
            if Utilities.sharedInstance.kDefaults.object(forKey: "BOY1_PURCHASED") != nil{
                btnBuy.setTitle("USE", for: UIControlState.normal)
                lblPrice.text = ""
            }
        case "BabyBoyTwoViewController":
            if Utilities.sharedInstance.kDefaults.object(forKey: "BOY2_PURCHASED") != nil{
                btnBuy.setTitle("USE", for: UIControlState.normal)
                lblPrice.text = ""
            }
        case "BabyBoyThreeViewController":
            if Utilities.sharedInstance.kDefaults.object(forKey: "BOY3_PURCHASED") != nil{
                btnBuy.setTitle("USE", for: UIControlState.normal)
                lblPrice.text = ""
            }
        case "BabyBoyFourViewController":
            if Utilities.sharedInstance.kDefaults.object(forKey: "BOY4_PURCHASED") != nil{
                btnBuy.setTitle("USE", for: UIControlState.normal)
                lblPrice.text = ""
            }
        case "BabyBoyFiveViewController":
            btnBuy.setTitle("USE", for: UIControlState.normal)
            lblPrice.text = ""
        case "BabyBoySixViewController":
            if Utilities.sharedInstance.kDefaults.object(forKey: "BOY6_PURCHASED") != nil{
                btnBuy.setTitle("USE", for: UIControlState.normal)
                lblPrice.text = ""
            }
        case "BabyBoySevenViewController":
            if Utilities.sharedInstance.kDefaults.object(forKey: "BOY7_PURCHASED") != nil{
                btnBuy.setTitle("USE", for: UIControlState.normal)
                lblPrice.text = ""
            }
        case "BabyBoyEightViewController":
            if Utilities.sharedInstance.kDefaults.object(forKey: "BOY8_PURCHASED") != nil{
                btnBuy.setTitle("USE", for: UIControlState.normal)
                lblPrice.text = ""
            }
        case "BabyBoyNineViewController":
            if Utilities.sharedInstance.kDefaults.object(forKey: "BOY9_PURCHASED") != nil{
                btnBuy.setTitle("USE", for: UIControlState.normal)
                lblPrice.text = ""
            }
        case "BabyBoyTextOneViewController":
            btnBuy.setTitle("USE", for: UIControlState.normal)
            lblPrice.text = ""
        case "BabyBoyTextTwoViewController":
            if Utilities.sharedInstance.kDefaults.object(forKey: "BOY11_PURCHASED") != nil{
                btnBuy.setTitle("USE", for: UIControlState.normal)
                lblPrice.text = ""
            }
        case "BabyGirlOneViewController":
            if Utilities.sharedInstance.kDefaults.object(forKey: "GIRL1_PURCHASED") != nil{
                btnBuy.setTitle("USE", for: UIControlState.normal)
                lblPrice.text = ""
            }else{
                print("nil")
            }
        case "BabyGirlTwoViewController":
            if Utilities.sharedInstance.kDefaults.object(forKey: "GIRL2_PURCHASED") != nil{
                btnBuy.setTitle("USE", for: UIControlState.normal)
                lblPrice.text = ""
            }
        case "BabyGirlThreeViewController":
            if Utilities.sharedInstance.kDefaults.object(forKey: "GIRL3_PURCHASED") != nil{
                btnBuy.setTitle("USE", for: UIControlState.normal)
                lblPrice.text = ""
            }
        case "BabyGirlFourViewController":
            if Utilities.sharedInstance.kDefaults.object(forKey: "GIRL4_PURCHASED") != nil{
                btnBuy.setTitle("USE", for: UIControlState.normal)
                lblPrice.text = ""
            }
            
        case "BabyGirlFiveViewController":
            btnBuy.setTitle("USE", for: UIControlState.normal)
            lblPrice.text = ""
        case "BabyGirlSixViewController":
            if Utilities.sharedInstance.kDefaults.object(forKey: "GIRL6_PURCHASED") != nil{
                btnBuy.setTitle("USE", for: UIControlState.normal)
                lblPrice.text = ""
            }
        case "BabyGirlSevenViewController":
            if Utilities.sharedInstance.kDefaults.object(forKey: "GIRL7_PURCHASED") != nil{
                btnBuy.setTitle("USE", for: UIControlState.normal)
                lblPrice.text = ""
            }
        case "BabyGirlEightViewController":
            if Utilities.sharedInstance.kDefaults.object(forKey: "GIRL8_PURCHASED") != nil{
                btnBuy.setTitle("USE", for: UIControlState.normal)
                lblPrice.text = ""
            }
        case "BabyGirlNineViewController":
            if Utilities.sharedInstance.kDefaults.object(forKey: "GIRL9_PURCHASED") != nil{
                btnBuy.setTitle("USE", for: UIControlState.normal)
                lblPrice.text = ""
            }
        case "BabyGirlTextOneViewController":
            btnBuy.setTitle("USE", for: UIControlState.normal)
            lblPrice.text = ""
        case "BabyGirlTextTwoViewController":
            if Utilities.sharedInstance.kDefaults.object(forKey: "GIRL11_PURCHASED") != nil{
                btnBuy.setTitle("USE", for: UIControlState.normal)
                lblPrice.text = ""
            }
        default:
            print("")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SKPaymentQueue.default().remove(self)
    }
    // MARK - IAP Methods
    func fetchAvailableProducts()  {
 
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.addLoadingView()
         }
        let category = Utilities.sharedInstance.kDefaults.object(forKey: "category") as! String
        switch category {
        case "BOY":
            let productIdentifiers = NSSet(objects:templateProductID!)
            productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers as! Set<String>)
            productsRequest.delegate = self
            productsRequest.start()

        case "GIRL":
            let productIdentifiers = NSSet(objects:templateProductID!)
            productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers as! Set<String>)
            productsRequest.delegate = self
            productsRequest.start()
        case  "OTHER":
            let productIdentifiers = NSSet(objects:templateProductID!)
            productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers as! Set<String>)
            productsRequest.delegate = self
            productsRequest.start()
            
        default:
            print("")
        }
    }
    
    // MARK: - REQUEST IAP PRODUCTS
    func productsRequest (_ request:SKProductsRequest, didReceive response:SKProductsResponse) {
        
        if (response.products.count > 0) {
            iapProducts = response.products
            print(response.products)
            self.purchaseMyProduct(product: iapProducts[0])
         }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert", message: "Purchases are disabled in your device!", controller: self)
        }
    }
    // MARK: - MAKE PURCHASE OF A PRODUCT
    func canMakePurchases() -> Bool {  return SKPaymentQueue.canMakePayments()  }
    func purchaseMyProduct(product: SKProduct) {
        if self.canMakePurchases() {
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(self)
            SKPaymentQueue.default().add(payment)
            print("PRODUCT TO PURCHASE: \(product.productIdentifier)")
            productID = product.productIdentifier
            
        } else {
            Utilities.displayFailureAlertWithMessage(title: "Alert", message: "Purchases are disabled in your device!", controller: self)
        }
    }
    // MARK:- IAP PAYMENT QUEUE
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        for transaction:AnyObject in transactions {
            if let trans = transaction as? SKPaymentTransaction {
                switch trans.transactionState {
                case .purchased:
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    DispatchQueue.main.async {
                        MBProgressHUD.hide(for: self.view, animated: true)

                        self.storePurchasedProductIDs(productID: self.productID)
                        self.delegate?.reloadCollectionView()
                        let VC1 = Utilities.sharedInstance.viewControllerWithName(identifier: Constants.StoryboardIdentifier.cardDetailsVC) as! CardDetailsViewController
                        let navController = UINavigationController(rootViewController: VC1)
                        self.present(navController, animated:true, completion: nil)
                    }
                    break
                case .failed:
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break
                case .restored:
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break
                default: break
                }}}
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func storePurchasedProductIDs(productID:String){
        
        switch productID {
        case BABY_BOY_1:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "BOY1_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        case BABY_BOY_2:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "BOY2_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        case BABY_BOY_3:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "BOY3_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        case BABY_BOY_4:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "BOY4_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        case BABY_BOY_5:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "BOY5_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        case BABY_BOY_6:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "BOY6_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        case BABY_BOY_7:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "BOY7_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        case BABY_BOY_8:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "BOY8_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        case BABY_BOY_9:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "BOY9_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        case BABY_BOY_10:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "BOY10_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        case BABY_BOY_11:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "BOY11_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        case BABY_GIRL_1:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "GIRL1_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        case BABY_GIRL_2:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "GIRL2_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        case BABY_GIRL_3:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "GIRL3_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        case BABY_GIRL_4:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "GIRL4_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        case BABY_GIRL_5:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "GIRL5_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        case BABY_GIRL_6:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "GIRL6_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        case BABY_GIRL_7:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "GIRL7_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        case BABY_GIRL_8:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "GIRL8_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        case BABY_GIRL_9:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "GIRL9_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        case BABY_GIRL_10:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "GIRL10_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        case BABY_GIRL_11:
            Utilities.sharedInstance.kDefaults.set(true, forKey: "GIRL11_PURCHASED")
            Utilities.sharedInstance.kDefaults.synchronize()
        default:
            print("")
        }
    }
    // MARK: - IBActions
    @IBAction func dismissButtonTapped(_ sender: Any) {
        dismiss(animated: true)
    }
    @IBAction func payanduseButtonTapped(_ sender: Any){
 
        let category = Utilities.sharedInstance.kDefaults.object(forKey: "category") as! String
        switch category {
        case "BOY":
                self.purchaseBoysTemplates()
        case "GIRL":
                self.purchaseGirlsTemplates()
        case "OTHER":
            if (templatePrice != "FREE" && btnBuy.title(for: .normal) != "USE"){
                    self.purchaseDefaultCategoryTemplates()
            }else{
                let VC1 = Utilities.sharedInstance.viewControllerWithName(identifier: Constants.StoryboardIdentifier.cardDetailsVC) as! CardDetailsViewController
                let navController = UINavigationController(rootViewController: VC1)
                self.present(navController, animated:true, completion: nil)
            }
        default:
            let VC1 = Utilities.sharedInstance.viewControllerWithName(identifier: Constants.StoryboardIdentifier.cardDetailsVC) as! CardDetailsViewController
            let navController = UINavigationController(rootViewController: VC1)
            self.present(navController, animated:true, completion: nil)
        }
    }
    func purchaseDefaultCategoryTemplates(){
        self.fetchAvailableProducts()
     }
    func purchaseBoysTemplates(){
        if templateIndex == 0 && !(Utilities.sharedInstance.kDefaults.object(forKey: "BOY1_PURCHASED") != nil){
            self.fetchAvailableProducts()

        }
        else if (templateIndex == 1 && !(Utilities.sharedInstance.kDefaults.object(forKey: "BOY2_PURCHASED") != nil)){
            self.fetchAvailableProducts()

        }
        else if (templateIndex == 2 && !(Utilities.sharedInstance.kDefaults.object(forKey: "BOY3_PURCHASED") != nil)){
            self.fetchAvailableProducts()

        }
        else if (templateIndex == 3 && !(Utilities.sharedInstance.kDefaults.object(forKey: "BOY4_PURCHASED") != nil)){
            self.fetchAvailableProducts()

        }
        else if (templateIndex == 5 && !(Utilities.sharedInstance.kDefaults.object(forKey: "BOY6_PURCHASED") != nil)){
            self.fetchAvailableProducts()

        }
        else if (templateIndex == 6 && !(Utilities.sharedInstance.kDefaults.object(forKey: "BOY7_PURCHASED") != nil)){
            self.fetchAvailableProducts()

        }
        else if (templateIndex == 7 && !(Utilities.sharedInstance.kDefaults.object(forKey: "BOY8_PURCHASED") != nil)){
            self.fetchAvailableProducts()

        }
        else if (templateIndex == 8 && !(Utilities.sharedInstance.kDefaults.object(forKey: "BOY9_PURCHASED") != nil)){
            self.fetchAvailableProducts()

        }
        else if (templateIndex == 10 && !(Utilities.sharedInstance.kDefaults.object(forKey: "BOY11_PURCHASED") != nil)){
            self.fetchAvailableProducts()

        }else{
            let VC1 = Utilities.sharedInstance.viewControllerWithName(identifier: Constants.StoryboardIdentifier.cardDetailsVC) as! CardDetailsViewController
            let navController = UINavigationController(rootViewController: VC1)
            self.present(navController, animated:true, completion: nil)
        }
    }
    func purchaseGirlsTemplates(){
        if ((templateIndex == 0 || templateIndex == 11) && !(Utilities.sharedInstance.kDefaults.object(forKey: "GIRL1_PURCHASED") != nil)){
            self.fetchAvailableProducts()
        }
        else if ((templateIndex == 1 || templateIndex == 12) && !(Utilities.sharedInstance.kDefaults.object(forKey: "GIRL2_PURCHASED") != nil)){
            self.fetchAvailableProducts()
        }
        else if ((templateIndex == 2 || templateIndex == 13) && !(Utilities.sharedInstance.kDefaults.object(forKey: "GIRL3_PURCHASED") != nil)){
            self.fetchAvailableProducts()
        }
        else if ((templateIndex == 3 || templateIndex == 14) && !(Utilities.sharedInstance.kDefaults.object(forKey: "GIRL4_PURCHASED") != nil)){
            self.fetchAvailableProducts()
        }
        else if ((templateIndex == 5 || templateIndex == 16) && !(Utilities.sharedInstance.kDefaults.object(forKey: "GIRL6_PURCHASED") != nil)){
            self.fetchAvailableProducts()
        }
        else if ((templateIndex == 6 || templateIndex == 17) && !(Utilities.sharedInstance.kDefaults.object(forKey: "GIRL7_PURCHASED") != nil)){
            self.fetchAvailableProducts()
        }
        else if ((templateIndex == 7 || templateIndex == 18) && !(Utilities.sharedInstance.kDefaults.object(forKey: "GIRL8_PURCHASED") != nil)){
            self.fetchAvailableProducts()
        }
        else if ((templateIndex == 8 || templateIndex == 19) && !(Utilities.sharedInstance.kDefaults.object(forKey: "GIRL10_PURCHASED") != nil)){
            self.fetchAvailableProducts()
        }
        else if ((templateIndex == 10 || templateIndex == 21 ) && !(Utilities.sharedInstance.kDefaults.object(forKey: "GIRL11_PURCHASED") != nil)){
             self.fetchAvailableProducts()
        }else{
            let VC1 = Utilities.sharedInstance.viewControllerWithName(identifier: Constants.StoryboardIdentifier.cardDetailsVC) as! CardDetailsViewController
            let navController = UINavigationController(rootViewController: VC1)
            self.present(navController, animated:true, completion: nil)
        }
    }
}
// MARK: - MIBlurPopupDelegate
extension PreviewViewController: MIBlurPopupDelegate {
    var popupView: UIView {
        return popupContentContainerView ?? UIView()
    }
    var blurEffectStyle: UIBlurEffectStyle {
        return customBlurEffectStyle
    }
    var initialScaleAmmount: CGFloat {
        return customInitialScaleAmmount
    }
    var animationDuration: TimeInterval {
        return customAnimationDuration
    }
}

