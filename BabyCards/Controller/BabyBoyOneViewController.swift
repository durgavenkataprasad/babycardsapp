//
//  BabyBoy1ViewController.swift
//  BabyCards
//
//  Created by Praveenkumar R on 10/07/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class BabyBoyOneViewController : UIViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var imageCard: UIImageView!
    @IBOutlet weak var cmVw: UIView!
    @IBOutlet weak var lblHours: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblSize: UILabel!
    @IBOutlet weak var lblDearlyLoved: UILabel!
    @IBOutlet weak var calenderVwEmbeddedVw1: UIView!
    @IBOutlet weak var scrollImg: UIScrollView!
    @IBOutlet weak var calenderVwEmbeddedVw2: UIView!
    @IBOutlet weak var lblBabyName: UILabel!
    @IBOutlet weak var calenderVw: UIView!
    @IBOutlet weak var rotateHeight: UIView!
    var babyInfo = [String:String]()
    var imagePicker = UIImagePickerController()
    @IBOutlet weak var lblCalenderMonth: UILabel!
    @IBOutlet weak var lblCalenderDate: UILabel!
    @IBOutlet weak var lblCalenderYear: YAxisLabel!
    var babyBornDateAndTime = String()
    @IBOutlet weak var screenShotVw: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setScrollVwDelegate()
        navigationItem.title = "Baby"
        self.DisplayBabyInfo(info: babyInfo)
        self.setBabyBornDateInCalender()
        babyBornDateAndTime = "\(babyInfo["babyDate"]! + " " + babyInfo["babyTime"]! + " " + babyInfo["babyTimeZone"]!)"
        self.roatateViewWithAngle()
        self.setBoarderColorCalenderStics()
    }
    func setScrollVwDelegate(){
        scrollImg.delegate = self
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageCard
    }
    func roatateViewWithAngle(){
        self.rotateHeight.transform = CGAffineTransform(rotationAngle: 0.2)
        self.calenderVw.transform = CGAffineTransform(rotationAngle: -0.2)
        self.calenderVwEmbeddedVw1.transform = CGAffineTransform(rotationAngle: -0.02)
        self.calenderVwEmbeddedVw2.transform = CGAffineTransform(rotationAngle: -0.02)
    }
    func setBoarderColorCalenderStics(){
        calenderVwEmbeddedVw1.layer.borderWidth = 2
        calenderVwEmbeddedVw1.layer.borderColor = UIColor.white.cgColor
        calenderVwEmbeddedVw1.layer.cornerRadius = 3
        calenderVwEmbeddedVw1.layer.masksToBounds = true
        calenderVwEmbeddedVw2.layer.borderWidth = 2
        calenderVwEmbeddedVw2.layer.borderColor = UIColor.white.cgColor
        calenderVwEmbeddedVw2.layer.cornerRadius = 3
        calenderVwEmbeddedVw2.layer.masksToBounds = true
        lblCalenderYear.adjustsFontSizeToFitWidth = true
    }
    func DisplayBabyInfo(info:[String:String]){
        // Height Type - INCHES/CM
        // Weight Type - POUNDS/KGS
        self.lblBabyName.text = info["babyName"] ?? ""
        self.lblDearlyLoved.text = info["parentName"] ?? ""
        self.lblHours.attributedText = Utilities.setAttributeStringWithSize(tittle: info["babyTime"]!, subTittle: info["babyTimeZone"]!, attributeTittleSize: 35, attributeSubtittleSize: 13, attributeColor: UIColor.black)
        self.lblSize.attributedText = Utilities.setAttributeStringWithSize(tittle: info["babyHeight"]!, subTittle: info["babyHeightType"]!, attributeTittleSize: 45, attributeSubtittleSize: 13, attributeColor: Constants.Boy1TintColor())
        self.lblWeight.attributedText = Utilities.setAttributeStringWithSize(tittle: info["babyWeight"]!, subTittle: info["babyWieghtType"]!, attributeTittleSize: 45, attributeSubtittleSize: 13, attributeColor: UIColor.black)
    }
    func setBabyBornDateInCalender(){
        let selectedDate = babyInfo["babyDate"] ?? ""
        let spiltArray = selectedDate.components(separatedBy: "/")
        self.lblCalenderDate.text = spiltArray[0]
        let monthNumber = Int(spiltArray[1])
        let fmt = DateFormatter()
        fmt.dateFormat = "MM"
        let month = fmt.monthSymbols[monthNumber! - 1]
         self.lblCalenderYear.text = spiltArray[2]
        self.lblCalenderMonth.text = month.uppercased()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    // MARK: - IBActions
    @IBAction func buttonTakePhotoTapped(_ sender: Any){
        openCamera()
    }
    @IBAction func buttonChoosePhotoTapped(_ sender: Any){
        openGallary()
    }
    @IBAction func buttonShareTapped(_ sender: Any){
        let image: UIImage? = screenShotVw.takeScreenshot()
        if image != nil {
            let imageToShare = [image!]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert!", message: "Please Take or Choose photo", controller: self)
        }
    }
}
extension BabyBoyOneViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
            
        }else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallary(){
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        //You will get cropped image here..
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.imageCard.image = image
        }else{
            print("Something went wrong")
        }
    }
}

