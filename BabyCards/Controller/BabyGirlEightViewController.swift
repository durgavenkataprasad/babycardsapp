//
//  BabyGirlEightViewController.swift
//  BabyCards
//
//  Created by CIPL108-MOBILITY on 23/08/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class BabyGirlEightViewController: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var imgGirlEight: UIImageView!
    @IBOutlet weak var screenShotView: UIView!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var scrollImg: UIScrollView!
    var imagePicker = UIImagePickerController()
    var girl8Info = [String:String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setScrollVwDelegate()
        self.dispalyGirlEightInfo()
    }
    func dispalyGirlEightInfo(){
        
        self.navigationItem.title = "Baby"
        self.lblName.text =  girl8Info["babyName"]!.uppercased()
        let formattedDate = Utilities.formattedDateFromString(dateString:girl8Info["babyDate"]!, withFormat: "MMM dd yyyy")
        let splitArray = formattedDate?.components(separatedBy: " ")
        self.lblDate.text = splitArray![1] + " " + splitArray![0].uppercased() + " " + splitArray![2]
        lblTime.text = girl8Info["babyTime"]! + girl8Info["babyTimeZone"]!
        
        let tintColor = UIColor(red:255/255.0, green:133/255.0 ,blue:151/255.0 , alpha:1)
        let weightString = "WEIGHING \(girl8Info["babyWeight"]! + girl8Info["babyWieghtType"]!)"
        let string_to_color = "\(girl8Info["babyWeight"]! + girl8Info["babyWieghtType"]!)"
        let range = (weightString as NSString).range(of: string_to_color)
        let attribute = NSMutableAttributedString.init(string: weightString)
        attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: tintColor , range: range)
        lblWeight.attributedText = attribute
        
        let heightString = "MEASURING \(girl8Info["babyHeight"]! + girl8Info["babyHeightType"]!)"
        let heightColorString = "\(girl8Info["babyHeight"]! + girl8Info["babyHeightType"]!)"
        let heightColorRange = (heightString as NSString).range(of: heightColorString)
        let heightAttribute = NSMutableAttributedString.init(string: heightString)
        heightAttribute.addAttribute(NSAttributedStringKey.foregroundColor, value: tintColor , range: heightColorRange)
        lblHeight.attributedText = heightAttribute
    }
    @IBAction func actionTakePhoto(_ sender: Any) {
        openCamera()
    }
    @IBAction func actionShare(_ sender: Any) {
        let image: UIImage? = screenShotView.takeScreenshot()
        if image != nil {
            let imageToShare = [ image! ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert!", message: "Please Take or Choose photo", controller: self)
        }
    }
    @IBAction func actionPhotoGallery(_ sender: Any) {
        openGallary()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func setScrollVwDelegate(){
        scrollImg.delegate = self
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgGirlEight
    }
}
extension BabyGirlEightViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    //MARK: - OpenCamera
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    //MARK: - OpenGallary
    func openGallary(){
        
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    //MARK: -UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        //You will get cropped image here..
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.imgGirlEight.image = image
        }else{
            print("Something went wrong")
        }
    }
}
