//
//  BabyBoyFourViewController.swift
//  BabyCards
//
//  Created by Praveenkumar R on 10/08/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class BabyBoyFourViewController: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var imgCard: UIImageView!
    @IBOutlet weak var lblBabyName: UILabel!
    @IBOutlet weak var lblBornTime: UILabel!
    @IBOutlet weak var lblBabyHeight: UILabel!
    @IBOutlet weak var lblBabyweight: UILabel!
    @IBOutlet weak var lblBornDate: UILabel!
    var babyInfo = [String:String]()
    var imagePicker = UIImagePickerController()
    @IBOutlet weak var scrollImg: UIScrollView!
    @IBOutlet weak var screenShotVw: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.DispalyBabyInfo()
        self.imgCard.layer.cornerRadius = self.imgCard.frame.height / 2
        self.imgCard.layer.masksToBounds = true
        self.setScrollVwDelegate()
    }
    func setScrollVwDelegate(){
        scrollImg.delegate = self
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgCard
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func DispalyBabyInfo(){
        self.navigationItem.title = "Baby"
        self.lblBabyName.text =  babyInfo["babyName"]!
        let formattedDate = Utilities.formattedDateFromString(dateString:babyInfo["babyDate"]!, withFormat: "MMM dd yyyy")
        let spiltArray = formattedDate?.components(separatedBy: " ")
        self.lblBornDate.text = spiltArray![0].uppercased() + " " + spiltArray![1] + " " + spiltArray![2]
        self.lblBornTime.text = babyInfo["babyTime"]! + babyInfo["babyTimeZone"]!
        if babyInfo["babyWeight"] == "KGS"{
            self.lblBabyweight.text = babyInfo["babyWeight"]! + " " + "POUNDS"
        }else{
            self.lblBabyweight.text = babyInfo["babyWeight"]! + " " + "KILOGRAMS"
        }
        if babyInfo["babyWeight"] == "CM"{
            self.lblBabyHeight.text = babyInfo["babyHeight"]! + " " + "INCHES"
        }else{
            self.lblBabyHeight.text = babyInfo["babyHeight"]! + " " + "CENTREMETRES"
        }
    }
    @IBAction func actionShare(_ sender: Any) {
        let image: UIImage? = screenShotVw.takeScreenshot()
        if image != nil {
            let imageToShare = [ image! ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert!", message: "Please Take or Choose photo", controller: self)
        }
    }
    @IBAction func actionPhotoLibrary(_ sender: Any) {
        openGallary()
    }
    @IBAction func actionTakePhoto(_ sender: Any) {
        openCamera()
    }
}
extension BabyBoyFourViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    //MARK: - OpenCamera
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    //MARK: - OpenGallary
    func openGallary(){
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    //MARK: -UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        //You will get cropped image here..
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.imgCard.image = image
        }else{
            print("Something went wrong")
        }
}
}

