//
//  BabyFrameViewController.swift
//  BabyCards
//
//  Created by CIPL108-MOBILITY on 03/08/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class BabyBoyThreeViewController: UIViewController,UIScrollViewDelegate {
    @IBOutlet weak var vWlayer: UIView!
    @IBOutlet weak var imgBaby: UIImageView!
    @IBOutlet weak var lblBabyBornDate: UILabel!
    @IBOutlet weak var lblBabyName: UILabel!
    @IBOutlet weak var lblBabyBornTime: UILabel!
    @IBOutlet weak var lblParentName: UILabel!
    @IBOutlet weak var lblBabyWeight: UILabel!
    @IBOutlet weak var lblBabyHeight: UILabel!
    var babyInfo = [String:String]()
    var imagePicker = UIImagePickerController()
    @IBOutlet weak var scrollImg: UIScrollView!
    @IBOutlet weak var screenShotView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.DisplayBabyInfo()
        self.setScrollVwDelegate()
    }
    func DisplayBabyInfo(){
        self.navigationItem.title = "Baby"
        self.lblBabyName.text =  babyInfo["babyName"] ?? ""
        let spiltArray = babyInfo["babyDate"]?.components(separatedBy: "/")
        let monthNumber = Int(spiltArray![1])
        let fmt = DateFormatter()
        fmt.dateFormat = "MM"
        let month = fmt.monthSymbols[monthNumber! - 1]

        self.lblBabyBornDate.text = month + " " + spiltArray![0] + " " + spiltArray![2]
        self.lblBabyBornTime.text = babyInfo["babyTime"]! + babyInfo["babyTimeZone"]!
        self.lblParentName.text = babyInfo["parentName"] ?? ""
        self.lblBabyWeight.text = babyInfo["babyWeight"]! + babyInfo["babyWieghtType"]!
        self.lblBabyHeight.text = babyInfo["babyHeight"]! + babyInfo["babyHeightType"]!
      }
    func setScrollVwDelegate(){
        scrollImg.delegate = self
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgBaby
    }
    @IBAction func actionShare(_ sender: Any) {
        let image:UIImage? = screenShotView.takeScreenshot()
        if image != nil {
            let imageToShare = [ image! ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert!", message: "Please Take or Choose photo", controller: self)
        }
    }
    @IBAction func actionTakePhoto(_ sender: Any) {
        openCamera()
    }
    @IBAction func actionPhotoLibrary(_ sender: Any) {
        openGallary()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension BabyBoyThreeViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    //MARK: - OpenCamera
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    //MARK: - OpenGallary
    func openGallary(){
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    //MARK: -UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        //You will get cropped image here..
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.imgBaby.image = image
        }else{
            print("Something went wrong")
        }
    }
}
