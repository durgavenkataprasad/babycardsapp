//
//  BabyGirlTextTwoViewController.swift
//  BabyCards
//
//  Created by CIPL108-MOBILITY on 23/08/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class BabyGirlTextTwoViewController: UIViewController {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var screenShotView: UIView!
    var girlTextTwoInfo = [String:String]()
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayGirlTextTwoInfo()
        
     }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func displayGirlTextTwoInfo(){
        self.navigationItem.title = "Baby"        
        let formattedDate = Utilities.formattedDateFromString(dateString:girlTextTwoInfo["babyDate"]!, withFormat: "dd MMM yyyy")
        let splitArray = formattedDate?.components(separatedBy: " ")
        self.lblDate.text = splitArray![0] + " " + splitArray![1].uppercased() + " " + splitArray![2]
        
        self.lblTime.text = girlTextTwoInfo["babyTime"]! + "\n" + girlTextTwoInfo["babyTimeZone"]!
        lblName.text = girlTextTwoInfo["babyName"]!.uppercased()
        self.lblWeight.text = girlTextTwoInfo["babyWeight"]! + "\n" + girlTextTwoInfo["babyWieghtType"]!
        self.lblHeight.text = girlTextTwoInfo["babyHeight"]! + "\n" + girlTextTwoInfo["babyHeightType"]!
    }
    @IBAction func actionShare(_ sender: Any) {
        let image: UIImage? = screenShotView.takeScreenshot()
        if image != nil {
            let imageToShare = [ image! ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert!", message: "Please Take or Choose photo", controller: self)
        }
    }
}
