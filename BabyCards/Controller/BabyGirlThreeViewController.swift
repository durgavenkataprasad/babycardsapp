//
//  BabyGirlThreeViewController.swift
//  BabyCards
//
//  Created by CIPL108-MOBILITY on 23/08/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class BabyGirlThreeViewController: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblParentName: UILabel!
    @IBOutlet weak var imgViewGirl3: UIImageView!
    @IBOutlet weak var screeShotView: UIView!
    @IBOutlet weak var scrollImg: UIScrollView!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    var girl3Info = [String:String]()
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayGirl3Info()
        self.setScrollVwDelegate()
    }
    func displayGirl3Info(){
        self.navigationItem.title = "Baby"
        self.lblName.text =  girl3Info["babyName"] ?? ""
        let array = girl3Info["babyDate"]?.components(separatedBy: "/")
        let monthNumber = Int(array![1])
        let fmt = DateFormatter()
        fmt.dateFormat = "MM"
        let month = fmt.monthSymbols[monthNumber! - 1]
        
        self.lblDate.text = month + " " + array![0] + " " + array![2]
        self.lblTime.text = girl3Info["babyTime"]! + girl3Info["babyTimeZone"]!
        self.lblParentName.text = girl3Info["parentName"] ?? ""
        self.lblWeight.text = girl3Info["babyWeight"]! + girl3Info["babyWieghtType"]!
        self.lblHeight.text = girl3Info["babyHeight"]! + girl3Info["babyHeightType"]!
    }
    func setScrollVwDelegate(){
        scrollImg.delegate = self
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgViewGirl3
    }
    @IBAction func actionShare(_ sender: Any) {
        let image: UIImage? = screeShotView.takeScreenshot()
        if image != nil {
            let imageToShare = [image!]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert!", message: "Please Take or Choose photo", controller: self)
        }
    }
    @IBAction func actionPhotoLibrary(_ sender: Any) {
        openGallary()
    }
    @IBAction func actionTakePhoto(_ sender: Any) {
        openCamera()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
extension BabyGirlThreeViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallary(){
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        //You will get cropped image here..
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.imgViewGirl3.image = image
        }else{
            print("Something went wrong")
        }
    }
}
