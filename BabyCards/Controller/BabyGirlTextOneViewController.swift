//
//  BabyGirlTextOneViewController.swift
//  BabyCards
//
//  Created by CIPL108-MOBILITY on 23/08/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class BabyGirlTextOneViewController: UIViewController {
    
    @IBOutlet weak var lblHeightType: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblWeightType: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var screenShotView: UIView!
    @IBOutlet weak var lblParentName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTimeZone: UILabel!
    @IBOutlet weak var mainVw: UIView!
    var girlTextOneInfo = [String:String]()
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayGirlTextOneInfo()
        self.setBoarderColor()
    }
    func setBoarderColor()
    {    mainVw.layer.borderWidth = 3.0
        mainVw.layer.borderColor = UIColor(red:255/255.0, green:133/255.0 ,blue:151/255.0 , alpha:1).cgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func displayGirlTextOneInfo(){
        self.navigationItem.title = "Baby"
        self.lblName.text =  girlTextOneInfo["babyName"]!
        self.lblParentName.text = girlTextOneInfo["parentName"]!.uppercased()
        
        let splitArray = girlTextOneInfo["babyDate"]?.components(separatedBy: "/")
        self.lblDate.text = splitArray![0] + "." + splitArray![1] + "." + splitArray![2]
        self.lblTime.text = girlTextOneInfo["babyTime"]!
        self.lblTimeZone.text = girlTextOneInfo["babyTimeZone"]!
        self.lblWeight.text = girlTextOneInfo["babyWeight"]!
        self.lblWeightType.text = girlTextOneInfo["babyWieghtType"]!
        self.lblHeight.text = girlTextOneInfo["babyHeight"]!
        self.lblHeightType.text = girlTextOneInfo["babyHeightType"]!
    }
    @IBAction func actionShare(_ sender: Any) {
        let image: UIImage? = screenShotView.takeScreenshot()
        if image != nil {
            let imageToShare = [ image! ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert!", message: "Please Take or Choose photo", controller: self)
        }
    }
    
}
