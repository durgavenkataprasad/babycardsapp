//
//  BabyBoyTwoViewController.swift
//  BabyCards
//
//  Created by CIPL108-MOBILITY on 13/08/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class BabyBoyTwoViewController: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var screenShotVw: UIView!
    @IBOutlet weak var lblBabyName: UILabel!
    @IBOutlet weak var lblBornDate: UILabel!
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    var babyInfo = [String:String]()
    var imagePicker = UIImagePickerController()
    @IBOutlet weak var scrollImg: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setScrollVwDelegate()
        self.DisplayBabyInfo()
    }
    func DisplayBabyInfo(){
        self.navigationItem.title = "Baby"
        self.lblBabyName.text =  babyInfo["babyName"]!.uppercased()
        let spiltArray = babyInfo["babyDate"]?.components(separatedBy: "/")
        self.lblBornDate.text = spiltArray![0] + "." + spiltArray![1] + "." +  spiltArray![2]
        self.lblTime.attributedText = Utilities.setAttributeStringWithSizeWithCustomFont(tittle: babyInfo["babyTime"]!, subTittle: babyInfo["babyTimeZone"]!, attributeTittleSize: 25, attributeSubtittleSize: 15, attributeColor: UIColor.white, font: "ImpactLTStd")
        
        self.lblWeight.attributedText = Utilities.setAttributeStringWithSizeWithCustomFont(tittle: babyInfo["babyWeight"]!, subTittle: babyInfo["babyWieghtType"]!, attributeTittleSize: 25, attributeSubtittleSize: 15, attributeColor: UIColor.white, font: "ImpactLTStd")

        self.lblHeight.attributedText = Utilities.setAttributeStringWithSizeWithCustomFont(tittle: babyInfo["babyHeight"]!, subTittle: babyInfo["babyHeightType"]!, attributeTittleSize: 25, attributeSubtittleSize: 15, attributeColor: UIColor.white, font: "ImpactLTStd")
     }
    @IBAction func actionTakePhoto(_ sender: Any) {
        openCamera()
    }
    func setScrollVwDelegate(){
        scrollImg.delegate = self
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgVw
    }
    @IBAction func actionShare(_ sender: Any) {
        let image: UIImage? = screenShotVw.takeScreenshot()
        if image != nil {
            let imageToShare = [ image! ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert!", message: "Please Take or Choose photo", controller: self)
        }
    }
    @IBAction func actionPhotoLibrary(_ sender: Any) {
        openGallary()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension BabyBoyTwoViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    //MARK: - OpenCamera
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    //MARK: - OpenGallary
    func openGallary(){
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    //MARK: -UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        //You will get cropped image here..
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.imgVw.image = image
        }else{
            print("Something went wrong")
        }
    }
}
