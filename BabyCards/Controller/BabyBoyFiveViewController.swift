//
//  BabyBoyFiveViewController.swift
//  BabyCards
//
//  Created by CIPL108-MOBILITY on 20/08/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class BabyBoyFiveViewController: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var scrollImg: UIScrollView!
    @IBOutlet weak var ImgBaby: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblBornDate: UILabel!
    var babyInfo = [String:String]()
    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var screenShotView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.DispalyBabyInfo()
        self.setScrollVwDelegate()
    }
    func setScrollVwDelegate(){
        scrollImg.delegate = self
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.ImgBaby
    }
    func DispalyBabyInfo(){
        self.navigationItem.title = "Baby"
        self.lblName.text =  babyInfo["babyName"]!
        let selectedDate = babyInfo["babyDate"] ?? ""
        let array = selectedDate.components(separatedBy: "/")
        let monthNumber = Int(array[1])
        let fmt = DateFormatter()
        fmt.dateFormat = "MM"
        let month = fmt.monthSymbols[monthNumber! - 1]
        self.lblBornDate.text =  month + " " + array[0] + " " + array[2]
        
        self.lblTime.attributedText = Utilities.setAttributeStringWithSizeWithCustomFont(tittle: babyInfo["babyTime"]!, subTittle: babyInfo["babyTimeZone"]!, attributeTittleSize: 20, attributeSubtittleSize: 13, attributeColor: UIColor.white, font: "JosefinSans-Bold")
        self.lblWeight.attributedText = Utilities.setAttributeStringWithSizeWithCustomFont(tittle: babyInfo["babyWeight"]!, subTittle: babyInfo["babyWieghtType"]!, attributeTittleSize: 20, attributeSubtittleSize: 13, attributeColor: UIColor.white, font: "JosefinSans-Bold")
        self.lblHeight.attributedText = Utilities.setAttributeStringWithSizeWithCustomFont(tittle: babyInfo["babyHeight"]!, subTittle: babyInfo["babyHeightType"]!, attributeTittleSize: 20, attributeSubtittleSize: 13, attributeColor: UIColor.white, font: "JosefinSans-Bold")
    }
    @IBAction func actionTakePhoto(_ sender: Any) {
        openCamera()
    }
    @IBAction func actionPhotoLibrary(_ sender: Any) {
        openGallary()
    }
    @IBAction func actionShare(_ sender: Any) {
        
        let image: UIImage? = screenShotView.takeScreenshot()
        if image != nil {
            let imageToShare = [ image! ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            Utilities.displayFailureAlertWithMessage(title: "Alert!", message: "Please Take or Choose photo", controller: self)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension BabyBoyFiveViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    //MARK: - OpenCamera
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    //MARK: - OpenGallary
    func openGallary(){
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    //MARK: -UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        //You will get cropped image here..
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.ImgBaby.image = image
        }else{
            print("Something went wrong")
        }
    }
}
