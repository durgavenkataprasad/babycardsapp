//
//  OvalView.swift
//  BabyCards
//
//  Created by Praveenkumar R on 11/08/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class OvalView: UIView {
  
  override func layoutSubviews() {
    super.layoutSubviews()
    layoutOvalMask()
  } 
  
  private func layoutOvalMask() {
    let mask = self.shapeMaskLayer()
    let bounds = self.bounds
    if mask.frame != bounds {
      mask.frame = bounds
      mask.path = CGPath(ellipseIn: bounds, transform: nil)
    }
  }
  
  private func shapeMaskLayer() -> CAShapeLayer {
    if let layer = self.layer.mask as? CAShapeLayer {
      return layer
    }
    let layer = CAShapeLayer()
    layer.fillColor = UIColor.black.cgColor
    self.layer.mask = layer
    return layer
  }
}
