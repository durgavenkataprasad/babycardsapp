//
//  Shared.swift
//  IMS
//
//  Created by Praveenkumar R on 12/04/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class Constants: NSObject {
    
    static func getCustomBoyColor() -> UIColor{
        return UIColor(red:128/255, green:188/255 ,blue:243/255 , alpha:1.00)
    }
    static func getCustomGirlColor() -> UIColor{
        return UIColor(red:219/255, green:155/255 ,blue:183/255 , alpha:1.00)
    }
    static func getCustomTwinsColor() -> UIColor{
        return UIColor(red:135/255, green:225/255 ,blue:206/255 , alpha:1.00)
    }
    static func getCustomFreeColor() -> UIColor{
        return UIColor(red:154/255, green:178/255 ,blue:229/255 , alpha:1.00)
    }
    static func getaquaMarineColor() -> UIColor{
        return UIColor(red:67/255, green:209/255 ,blue:194/255 , alpha:1.00)
    }
    static func getwarmGreyColor() -> UIColor{
        return UIColor(red:137/255, green:137/255 ,blue:137/255 , alpha:1.00)
    }
    static func babyMeasurementsColor() -> UIColor{
        return UIColor(red:102/255, green:59/255 ,blue:24/255 , alpha:1.00)
    }
    static func babyHeightMeasureColor() -> UIColor{
        return UIColor(red:243/255, green:36/255 ,blue:121/255 , alpha:1.00)
    }
    static func Boy1TintColor() -> UIColor{
        return UIColor(red:0/255, green:120/255 ,blue:183/255 , alpha:1.00)
    }
    
    static func GirlOneTemplateTextTintColor() -> UIColor{
        return UIColor(red:103/255, green:55/255 ,blue:24/255 , alpha:1.00)
    }
    
    static func BoySixTemplateTextTintColor() -> UIColor{
        return UIColor(red:128/255, green:222/255 ,blue:228/255 , alpha:1.00)
    }
    
    struct StoryboardIdentifier {
        // Boys
        static let previewVC      = "PreviewVC"
        static let cardDetailsVC  = "CardDetails"
        static let cardBoyOne    = "CardImage"
        static let CardBoyTwo    = "CardBoyTwo"
        static let CardBoyThree    = "CardBoyThree"
        static let CardBoyFour    = "CardBoyFour"
        static let CardTextOne    = "BoyTextOne"
        static let CardTextTwo    = "BoyTextTwo"
        static let CardBoyFive    = "CardBoyFive"
        static let CardBoySix    = "CardBoySix"
        static let CardBoySeven    = "CardBoySeven"
        static let CardBoyEight    = "CardBoyEight"
        static let CardBoyNine    = "CardBoyNine"
        // Girls
        static let GirlOne    = "GirlOne"
        static let GirlTwo    = "GirlTwo"
        static let GirlThree    = "GirlThree"
        static let GirlFour    = "GirlFour"
        static let GirlFive    = "GirlFive"
        static let GirlSix    = "GirlSix"
        static let GirlSeven    = "GirlSeven"
        static let GirlEight    = "GirlEight"
        static let GirlNine    = "GirlNine"
        static let GirlTextOne    = "GirlTextOne"
        static let GirlTextTwo    = "GirlTextTwo"
    }
    struct ErrorMessage {
    }
}
