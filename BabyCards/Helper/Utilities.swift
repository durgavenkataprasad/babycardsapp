//
//  Utilities.swift
//  MTopo
//
//  Created by CIPL137-MOBILITY on 13/02/17.
//  Copyright © 2017 Colan Infotech Pvt Ltd. All rights reserved.
//

import UIKit
import StoreKit

class Utilities: NSObject {
    
    static let sharedInstance = Utilities()
    var kDefaults = UserDefaults.standard
    
 
    func viewControllerWithName(identifier: String) ->UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: identifier)
    }
    
    // MARK: - Alert Methods
    class func displayFailureAlertWithMessage(title : String, message: String, controller : UIViewController){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle:.alert)
        let alertOKButton = UIAlertAction(title: "OK", style:.cancel, handler: nil)
        alert.addAction(alertOKButton)
        controller.present(alert, animated: true, completion: {
        })
    }
    func trimSpaces(theString: String) -> String {
        
        let trimmedString = theString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return trimmedString
    }
    func isFieldEmpty(theString: String) -> Bool {
        
        if self.trimSpaces(theString: theString).characters.count <= 0 {
            return true
        }
        else
        {
            return false
        }
    }
    class func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    class func showSuccessFailureAlertWithDismissHandler(title : String, message: String, controller : UIViewController, alertDismissed:@escaping ((_ okPressed: Bool)->Void)){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertOKButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            alertDismissed(true)
        })
        alert.addAction(alertOKButton)
        controller.present(alert, animated: true, completion: {
            
        })
    }
    class func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "dd/MM/yyyy"
        if let date = inputFormatter.date(from: dateString) {
            
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            return outputFormatter.string(from: date)
        }
        return nil
    }
    class func setAttributeStringWithSize(tittle:String,subTittle:String, attributeTittleSize:CGFloat, attributeSubtittleSize:CGFloat, attributeColor:UIColor) -> NSMutableAttributedString{
        
        let attributeTittle = [NSAttributedStringKey.foregroundColor: attributeColor, NSAttributedStringKey.font: UIFont (name: "Sketch Rockwell", size: attributeTittleSize)]
        let attributeSubTittle = [NSAttributedStringKey.foregroundColor: attributeColor, NSAttributedStringKey.font: UIFont (name: "HelveticaNeue-Bold", size: attributeSubtittleSize)]
        let tittleText = NSMutableAttributedString(string: tittle, attributes: attributeTittle)
        let subTittleText = NSMutableAttributedString(string: subTittle, attributes: attributeSubTittle)
        
        let attributeString = NSMutableAttributedString()
        attributeString.append(tittleText)
        attributeString.append(subTittleText )
        return attributeString
    }
    
    class func setAttributeStringWithSizeWithCustomFont(tittle:String,subTittle:String, attributeTittleSize:CGFloat, attributeSubtittleSize:CGFloat, attributeColor:UIColor, font:String) -> NSMutableAttributedString{
        
        let attributeTittle = [NSAttributedStringKey.foregroundColor: attributeColor, NSAttributedStringKey.font: UIFont (name: font, size: attributeTittleSize)]
        let attributeSubTittle = [NSAttributedStringKey.foregroundColor: attributeColor, NSAttributedStringKey.font: UIFont (name: font, size: attributeSubtittleSize)]
        let tittleText = NSMutableAttributedString(string: tittle, attributes: attributeTittle)
        let subTittleText = NSMutableAttributedString(string: subTittle, attributes: attributeSubTittle)
        
        let attributeString = NSMutableAttributedString()
        attributeString.append(tittleText)
        attributeString.append(subTittleText )
        return attributeString
    }
    
    func shuffledInputArray() -> [[String:Any]]{
        var shuuffleArr = [[String:Any]]()
        let Boy1 = [
            "image": "BabyBoy1",
            "price": "$0.99",
            "preview_image": "BabyB1",
            "vcName": "BabyBoyOneViewController",
            "category": "BOY",
            "product_id": "ba_c001",
            "isPurchased": "BOY1_PURCHASED"
            ] as [String : Any]
        let Boy2 = [
            "image": "Baby_Boy2",
            "price": "$0.99",
            "preview_image":"Baby2Boy",
            "vcName": "BabyBoyTwoViewController",
            "category": "BOY",
            "product_id": "ba_c002",
            "isPurchased": "BOY2_PURCHASED"
             ] as [String : Any]
        let Boy3 = [
            "image": "BabyBoy3",
            "price": "$0.99",
            "preview_image":"BabyBoy3",
            "vcName": "BabyBoyThreeViewController",
            "category": "BOY",
            "product_id": "ba_c003",
            "isPurchased": "BOY3_PURCHASED"
            ] as [String : Any]
        let Boy4 = [
            "image": "Baby4Boy",
            "price": "$0.99",
            "preview_image":"Baby_Boy4",
            "vcName": "BabyBoyFourViewController",
            "category": "BOY",
            "product_id": "ba_c004",
            "isPurchased": "BOY4_PURCHASED"
            ] as [String : Any]
        let Boy5 = [
            "image": "Baby5Boy",
            "price": "FREE",
            "preview_image":"Baby5B_1",
            "vcName": "BabyBoyFiveViewController",
            "category": "BOY",
            "product_id": "",
            "isPurchased": "BOY5_PURCHASED"
            ] as [String : Any]
        let Boy6 = [
            "image": "Baby6Boy",
            "price": "$0.99",
            "preview_image":"Baby6B",
            "vcName": "BabyBoySixViewController",
            "category": "BOY",
            "product_id": "ba_c005",
            "isPurchased": "BOY6_PURCHASED"
            ] as [String : Any]
        let Boy7 = [
            "image": "Baby7Boy",
            "price": "$0.99",
            "preview_image":"Baby7B",
            "vcName": "BabyBoySevenViewController",
            "category": "BOY",
            "product_id": "ba_c006",
            "isPurchased": "BOY7_PURCHASED"
            ] as [String : Any]
        let Boy8 = [
            "image": "Baby8Boy",
            "price": "$0.99",
            "preview_image":"Baby_Boy8",
            "vcName": "BabyBoyEightViewController",
            "category": "BOY",
            "product_id": "ba_c007",
            "isPurchased": "BOY8_PURCHASED"
            ] as [String : Any]
        let Boy9 = [
            "image": "Baby9Boy",
            "price": "$0.99",
            "preview_image":"Baby9B",
            "vcName": "BabyBoyNineViewController",
            "category": "BOY",
            "product_id": "ba_c008",
            "isPurchased": "BOY9_PURCHASED"
            ] as [String : Any]
        let BoyTextOne = [
            "image": "BabyText1Boy",
            "price": "FREE",
            "preview_image":"BabyTxt1B",
            "vcName": "BabyBoyTextOneViewController",
            "category": "BOY",
            "product_id": "",
            "isPurchased": "BOY10_PURCHASED"
            ] as [String : Any]
        let BoyTextTwo = [
            "image": "BabyText2",
            "price": "$0.99",
            "preview_image":"BabyTxt2B",
            "vcName": "BabyBoyTextTwoViewController",
            "category": "BOY",
            "product_id": "ba_c009",
            "isPurchased": "BOY11_PURCHASED"
            ] as [String : Any]
        let Girl1 = [
            "image": "Baby1Girl",
            "price": "$0.99",
            "preview_image":"Baby1Girl",
            "vcName": "BabyGirlOneViewController",
            "category": "GIRL",
            "product_id": "ba_c0010",
            "isPurchased": "GIRL1_PURCHASED"
            ] as [String : Any]
        let Girl2 = [
            "image": "Baby_Girl2",
            "price": "$0.99",
            "preview_image":"Baby2Girl",
            "vcName": "BabyGirlTwoViewController",
            "category": "GIRL",
            "product_id": "ba_c0011",
            "isPurchased": "GIRL2_PURCHASED"
            ] as [String : Any]
        let Girl3 = [
            "image": "Baby3Girl",
            "price": "$0.99",
            "preview_image":"Baby3Girl",
            "vcName": "BabyGirlThreeViewController",
            "category": "GIRL",
            "product_id": "ba_c0012",
            "isPurchased": "GIRL3_PURCHASED"
            ] as [String : Any]
        let Girl4 = [
            "image": "Baby4Girl",
            "price": "$0.99",
            "preview_image":"Baby4Girl",
            "vcName": "BabyGirlFourViewController",
            "category": "GIRL",
            "product_id": "ba_c0013",
            "isPurchased": "GIRL4_PURCHASED"
            ] as [String : Any]
        let Girl5 = [
            "image": "Baby5Girl",
            "price": "FREE",
            "preview_image":"Baby5G_1",
            "vcName": "BabyGirlFiveViewController",
            "category": "GIRL",
            "product_id": "",
            "isPurchased": "GIRL5_PURCHASED"
            ] as [String : Any]
        let Girl6 = [
            "image": "Baby6Girl",
            "price": "$0.99",
            "preview_image":"Baby6G",
            "vcName": "BabyGirlSixViewController",
            "category": "GIRL",
            "product_id": "ba_c0014",
            "isPurchased": "GIRL6_PURCHASED"
            ] as [String : Any]
        let Girl7 = [
            "image": "Baby7Girl",
            "price": "$0.99",
            "preview_image":"Baby7G",
            "vcName": "BabyGirlSevenViewController",
            "category": "GIRL",
            "product_id": "ba_c0015",
            "isPurchased": "GIRL7_PURCHASED"
            ] as [String : Any]
        let Girl8 = [
            "image": "Baby8Girl",
            "price": "$0.99",
            "preview_image":"Baby8G",
            "vcName": "BabyGirlEightViewController",
            "category": "GIRL",
            "product_id": "ba_c0016",
            "isPurchased": "GIRL8_PURCHASED"
            ] as [String : Any]
        let Girl9 = [
            "image": "Baby9Girl",
            "price": "$0.99",
            "preview_image":"Baby9G",
            "vcName": "BabyGirlNineViewController",
            "category": "GIRL",
            "product_id": "ba_c0017",
            "isPurchased": "GIRL9_PURCHASED"
            ] as [String : Any]
        let GirlTextOne = [
            "image": "BabyText1Girl",
            "price": "FREE",
            "preview_image":"BabyTxt1G",
            "vcName": "BabyGirlTextOneViewController",
            "category": "GIRL",
            "product_id": "",
            "isPurchased": "GIRL10_PURCHASED"
            ] as [String : Any]
        let GirlTextTWo = [
            "image": "BabyText2Girl",
            "price": "0.99",
            "preview_image":"BabyTxt2G",
            "vcName": "BabyGirlTextTwoViewController",
            "category": "GIRL",
            "product_id": "ba_c0018",
            "isPurchased": "GIRL11_PURCHASED"
            ] as [String : Any]

        shuuffleArr.append(Boy1)
        shuuffleArr.append(Boy2)
        shuuffleArr.append(Boy3)
        shuuffleArr.append(Boy4)
        shuuffleArr.append(Boy5)
        shuuffleArr.append(Boy6)
        shuuffleArr.append(Boy7)
        shuuffleArr.append(Boy8)
        shuuffleArr.append(Boy9)
         shuuffleArr.append(BoyTextOne)
        shuuffleArr.append(BoyTextTwo)

        shuuffleArr.append(Girl1)
        shuuffleArr.append(Girl2)
        shuuffleArr.append(Girl3)
        shuuffleArr.append(Girl4)
        shuuffleArr.append(Girl5)
        shuuffleArr.append(Girl6)
        shuuffleArr.append(Girl7)
        shuuffleArr.append(Girl8)
        shuuffleArr.append(Girl9)
         shuuffleArr.append(GirlTextOne)
        shuuffleArr.append(GirlTextTWo)
        
        return shuuffleArr
    }
    
}

