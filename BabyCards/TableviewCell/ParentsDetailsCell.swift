//
//  ParentsDetailsCell.swift
//  BabyCards
//
//  Created by Praveenkumar R on 10/07/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

class ParentsDetailsCell: UITableViewCell {
  
    @IBOutlet var buttonChoosePhoto: UIButton!
    @IBOutlet weak var txtParentName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassCode: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
