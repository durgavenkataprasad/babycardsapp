//
//  CustomBabyDetailsCell.swift
//  BabyCards
//
//  Created by Praveenkumar R on 09/07/18.
//  Copyright © 2018 Praveenkumar R. All rights reserved.
//

import UIKit

enum MyTheme {
    case light
    case dark
}

class CustomBabyDetailsCell: UITableViewCell ,CalenderDelegate {
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var parentNameVw: UIView!
    @IBOutlet var txtBabyName: UITextField!
    @IBOutlet var txtBabyWeight: UITextField!
    @IBOutlet var txtBabyHeight: UITextField!
    @IBOutlet var txtBabyTime: UITextField!

    @IBOutlet var buttonLBS: UIButton!
    @IBOutlet var buttonKGS: UIButton!
    @IBOutlet var buttonINCH: UIButton!
    @IBOutlet var buttonCM: UIButton!
    @IBOutlet var buttonAM: UIButton!
    @IBOutlet var buttonPM: UIButton!
    
    @IBOutlet weak var txtParentName: UITextField!
    @IBOutlet var viewCalenderView: UIView!
    
    var selctedDate : String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if Utilities.sharedInstance.kDefaults.object(forKey: "isHide") as! Bool == true{
                heightConstraint.constant = 0
                parentNameVw.isHidden = true
         }
        
        viewCalenderView.addSubview(calenderView)
        calenderView.delegate = self
        calenderView.topAnchor.constraint(equalTo: viewCalenderView.topAnchor, constant: 10).isActive=true
        calenderView.rightAnchor.constraint(equalTo: viewCalenderView.rightAnchor, constant: -12).isActive=true
        calenderView.leftAnchor.constraint(equalTo: viewCalenderView.leftAnchor, constant: 12).isActive=true
        calenderView.heightAnchor.constraint(equalToConstant: 365).isActive=true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    lazy var  calenderView: CalenderView = {
        let calenderView = CalenderView(theme: MyTheme.light)
        calenderView.translatesAutoresizingMaskIntoConstraints=false
        return calenderView
    }()
    
    func didTapDate(date: String, available: Bool) {
        if available == true {
            self.selctedDate = date
            let dateDataDict:[String: String] = ["selectedDate": self.selctedDate!]

            NotificationCenter.default.post(name: Notification.Name("reloadTableVw"), object: nil, userInfo: dateDataDict)

            print(date)
        } else {
            showAlert()
            self.selctedDate = date
        }
    }
    fileprivate func showAlert(){
        
    }
}
